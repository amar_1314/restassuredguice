package helper;

import com.github.javafaker.Faker;

import java.util.List;
import java.util.stream.Collectors;

public class HelperFunctions {

    public static String getValidPhoneNumber() {
        String validNumber;
        Faker faker = new Faker();
        for (int i = 0; i < 10; i++) {
            validNumber = faker.phoneNumber().cellPhone();
            validNumber = Formatter.tenDigitNumber.format(validNumber);
            if (validNumber.matches(Regex.validPhoneNumber)) {
                return validNumber;
            }
        }
        return null;
    }

    public static String prettyFormat(Object object) {
        if (object instanceof List) {
            return (String) ((List) object).stream().map(o -> o.toString()).collect(Collectors.joining("</br>"));
        }
        return object.toString();
    }
}
