package helper;

@FunctionalInterface
public interface IStringFormat {
    String format(String number);
}
