package helper;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.Locale;
import java.util.function.BiFunction;

public class Formatter {

    public static IStringFormat tenDigitNumber = (number) -> number.replaceAll(Regex.anythingOtherThanNumber, "");
    public static IStringFormat fpEmail = (email) -> email.replaceAll(Regex.anythingOtherThanFpEmail, "fp");
    public static IPlainPriceFormat plainPriceFormat = Formatter::getPlainPriceFromFormattedPrice;
    private static NumberFormat instance = NumberFormat.getCurrencyInstance(Locale.US);
    public static ICurrency usCurrency = (value) -> value instanceof String
            ? instance.format(Double.parseDouble(value.toString().replaceAll(Regex.anythingOtherThanNumberAndDecimalPoint, "").trim()))
            : instance.format(value);
    private static HashMap<DayOfWeek, BiFunction<DayOfWeek, LocalDate, LocalDate>> weekDayToRegularDateProvider;
    public static IDateFormat stringDateToRegularDate = Formatter::getRegularDateFromStringDate;

    private Formatter() {
        weekDayToRegularDateProvider = new HashMap<>();
        weekDayToRegularDateProvider.put(DayOfWeek.MONDAY, this::getFormattedDate);
        weekDayToRegularDateProvider.put(DayOfWeek.TUESDAY, this::getFormattedDate);
        weekDayToRegularDateProvider.put(DayOfWeek.WEDNESDAY, this::getFormattedDate);
        weekDayToRegularDateProvider.put(DayOfWeek.THURSDAY, this::getFormattedDate);
        weekDayToRegularDateProvider.put(DayOfWeek.FRIDAY, this::getFormattedDate);
        weekDayToRegularDateProvider.put(DayOfWeek.SATURDAY, this::getFormattedDate);
        weekDayToRegularDateProvider.put(DayOfWeek.SUNDAY, this::getFormattedDate);
    }

    private static LocalDate getRegularDateFromStringDate(@NotNull String stringDate, LocalDate startingDate) {
        new Formatter();
        if (stringDate.toUpperCase().contains("TODAY")) {
            return getRegularDateFromTodayStringDate(stringDate);
        } else {
            return getRegularDateFromWeekDayString(stringDate, startingDate);
        }

//        fail("Unknown date format: "+stringDate);
//        return null;
    }

    private static double getPlainPriceFromFormattedPrice(String formattedPrice) {
        String plainNumberString = formattedPrice.replaceAll(Regex.anythingOtherThanNumberAndDecimalPoint, "").trim();
        if (plainNumberString.isEmpty()) {
            return 0.0;
        }

        return Double.parseDouble(formattedPrice.replaceAll(Regex.anythingOtherThanNumberAndDecimalPoint, "").trim());
    }

    private static LocalDate getRegularDateFromWeekDayString(String stringDate, LocalDate startingDate) {

        DayOfWeek dayOfWeek = DayOfWeek.valueOf(stringDate.toUpperCase().trim());
        return weekDayToRegularDateProvider.get(dayOfWeek).apply(dayOfWeek, startingDate);
    }

    private static LocalDate getRegularDateFromTodayStringDate(String stringDate) {
        if (stringDate.contains("+")) {
            int additionalDays = Integer.parseInt(stringDate.split("\\+")[1].trim());
            return LocalDate.now().plusDays(additionalDays);
        } else if (stringDate.contains("-")) {
            int minusDays = Integer.parseInt(stringDate.split("-")[1].trim());
            return LocalDate.now().minusDays(minusDays);
        } else {
            return LocalDate.now();
        }
    }

    private LocalDate getFormattedDate(DayOfWeek dayOfWeek, LocalDate startingDate) {

        return LocalDate.of(startingDate.getYear(), startingDate.getMonth(), startingDate.getDayOfMonth()).with(TemporalAdjusters.next(dayOfWeek));
    }

}
