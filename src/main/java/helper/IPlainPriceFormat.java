package helper;

@FunctionalInterface
public interface IPlainPriceFormat {
    double format(String stringPrice);
}
