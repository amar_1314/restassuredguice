package helper;

public class Regex {
    public static final String validPhoneNumber = "^[2-9][0-8][0-9]-?[2-9][0-9]{2}-?[0-9]{4}$";
    static final String anythingOtherThanNumberAndDecimalPoint = "[^0-9.]";
    static final String anythingOtherThanNumber = "[^0-9]";
    static final String anythingOtherThanFpEmail = "gmail|yahoo|hotmail";
}
