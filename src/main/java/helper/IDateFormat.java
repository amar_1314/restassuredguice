package helper;

import java.time.LocalDate;

@FunctionalInterface
public interface IDateFormat {
    //Takes string date as input and a starting date if you want to set starting date to some day in future or in past
    LocalDate format(String stringDate, LocalDate startingDate);
}
