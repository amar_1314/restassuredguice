package api_request_resources;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

@Data
@ScenarioScoped
public class EcomQuoteRequest {

    private int quoteId;
    private String partnerLeadId;
    private int contractTypeID;
    private int planId;
    private Shipping shipping;
    private Coupon[] coupons;
    private double contractDiscount = 0;
    private double subTotal = 0;
    private double total = 0;
    private double tax = 0;
    private Item[] items;


    @Data
    public static class Shipping {
        private int shippingTypeId;
        private double shippingCost = 0;
        private double shippingDiscount = 0;
    }

    @Data
    public static class Coupon {
        private String couponCode;
        private double couponAmount = 0;
        private int offerId = 0;
    }

    @Data
    public static class Item {
        private int productId;
        private String productSku;
        private double productCost;
        private double productDiscount;
        private double productTax;
    }


}
