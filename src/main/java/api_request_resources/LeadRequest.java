package api_request_resources;

import dto.NewAddress;
import lombok.Data;

@Data
public class LeadRequest {

    private NewAddress address;
    private String companyName = "";
    private Compliance compliance;
    private String email;
    private String firstName;
    private String lastName;
    private int leadSourceId = 1533;
    private String phone;
    private String subId = "";

    @Data
    public static class Compliance {
        private boolean tcpaPermissionGranted;
    }
}


