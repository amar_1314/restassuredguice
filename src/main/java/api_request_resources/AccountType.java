package api_request_resources;

import lombok.Data;

@Data
public class AccountType {
    private String accountType;
}
