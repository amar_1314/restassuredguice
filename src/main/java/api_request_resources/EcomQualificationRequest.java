package api_request_resources;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cucumber.runtime.java.guice.ScenarioScoped;
import dto.Address;
import dto.Qualification;
import lombok.Data;

@Data
@ScenarioScoped
public class EcomQualificationRequest {

    private Address address;
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private boolean isOwnerOfProperty;
    private String phoneNumber;
    private String isCreditOverride;
    @JsonIgnore
    private Qualification qualification;
}
