package api_request_resources;

public enum ContractType {
    OneYear(1),
    TwoYear(2),
    ThreeYear(3),
    MonthToMonth(4);

    private int contractTypeId;

    ContractType(int contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public int getContractTypeId() {
        return contractTypeId;
    }


}
