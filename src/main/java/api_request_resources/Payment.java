package api_request_resources;

import dto.Address;
import lombok.Data;

@Data
class Payment {
    private Address address;
    private Integer addressId;
    private String altEmail;
    private String altPhone;
    private String attention;
    private String cardNumber;
    private String city;
    private String companyName;
    private Integer contactId;
    private String email;
    private String expirationDate;
    private String expirationMonth;
    private String expirationYear;
    private String firstName;
    private String lastName;
    private boolean isResidential;
    private boolean isValid;
    private String line1;
    private String line2;
    private String paymentMethod;
    private String phone;
    private String postalCode;
    private String state;
    private String verificationNumber;
}
