package api_request_resources;

public enum MonitoringPlan {
    IM(1),
    CO(2),
    PM(4),
    UM(5);

    private int monitoringPlanId;

    MonitoringPlan(int monitoringPlanId) {
        this.monitoringPlanId = monitoringPlanId;
    }

    public int getMonitoringPlanId() {
        return monitoringPlanId;
    }
}
