package api_request_resources;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

@Data
@ScenarioScoped
public class EcomLeadRequest {

    private String partnerLeadId;
    private String firstName;
    private String lastName;
    private String companyName = "";
    private String emailAddress;
    private String secondaryEmailAddress;
    private String phone;
    private String secondaryPhone;
    private boolean isResidential;
    private String line1;
    private String line2 = "";
    private String city;
    private String state;
    private String postalCode;
    private LeadSourceInfo leadSourceInfo;
    private AdditionalFields additionalFields;
    @JsonIgnore
    private int leadId;

    @Data
    private static class LeadSourceInfo {
        private String utmSource;
        private String utmMedium;
        private String utmTerm;
        private String utmContent;
        private String utmCampaign;
        private String utmClId;
        private String utmClientId;
    }

    @Data
    private static class AdditionalFields {
        private String additionalProp1;
        private String additionalProp2;
        private String additionalProp3;

    }
}
