package api_request_resources;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

@Data
@ScenarioScoped
class BankAccountInfo {
    private String accountNumber;
    private String accountType;
    private String routingNumber;
}
