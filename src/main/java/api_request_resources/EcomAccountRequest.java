package api_request_resources;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

@Data
@ScenarioScoped
public class EcomAccountRequest {

    private int quoteId;
    private AlarmInformation alarmInformation;
    private int shippingContactId;
    private int premisesContactId;
    @JsonIgnore
    private int accountId;
}
