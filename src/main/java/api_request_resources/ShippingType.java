package api_request_resources;

public enum ShippingType {

    Ground(1),
    TwoDay(2),
    Overnight(3),
    Saturday(4);

    private int shippingTypeId;

    ShippingType(int shippingTypeId) {
        this.shippingTypeId = shippingTypeId;
    }

    public int getShippingTypeId() {
        return shippingTypeId;
    }

}
