package api_request_resources;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ScenarioScoped
public class CreditCardInfo {

    private String cardnumber;
    private LocalDateTime expirationDate;
    private String cvv;
}
