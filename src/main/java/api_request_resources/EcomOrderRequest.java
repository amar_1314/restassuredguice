package api_request_resources;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import java.util.Map;

@Data
@ScenarioScoped
public class EcomOrderRequest {

    private String partnerLeadId;
    private int quoteId;
    private int shippingContactId;
    private int paymentContactId;
    private CreditCardInfo upfrontPaymentInformation;
    private BankAccountInfo recurringPaymentInformation;
    private CreditCardInfo recurringPaymentInformationCredit;
    private double tax;
    private double total;
    private Map<String, String> orderAttributes;
    @JsonIgnore
    private int orderId;


}
