package api_request_resources;

import lombok.Data;

@Data
public class AlarmInformation {

    private String initialUserCode = "";
    private String passCode;
}
