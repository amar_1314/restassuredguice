package api_request_resources;

import dto.Address;
import dto.Contact;
import dto.Lead;
import dto.Quote;
import lombok.Data;

@Data
public class InitialOrderRequest {

    private AlarmInformation alarmInformation;
    private Contact contactInformation;
    private double estimatedSalesTax;
    private Lead lead;
    private Integer leadId;
    private boolean paymentAddressUsePremisesAddress;
    private String[] paymentTypes;
    private Address premisesAddress;
    private Integer qualificationId;
    private Quote quote;
    private Integer quoteId;
    private Payment recurringPayment;
    private Address recurringPaymentAddress;
    private Contact recurringPaymentContactInfo;
    private AccountType accountType;
    private Payment recurringPaymentInformationCredit;
    private String recurringPaymentMethod;
    private boolean recurringPaymentUseInitialPayment;
    private Address shippingAddress;
    private boolean shippingAddressUsePremisesAddress;
    private Address upFrontPaymentAddress;
    private Contact upFrontPaymentContactInfo;
    private Payment upFrontPaymentInformation;
    private boolean chargeFullAmountInProgress;
    private boolean formLocked;
    private boolean invalidRoutingNumber;
    private boolean isCameraPlan;
    private boolean isCanadianPremises;
    private boolean isValidOrder;
    private String rmrStartDate;


}
