package reporting;

import gherkin.formatter.model.Tag;

import java.util.ArrayList;
import java.util.List;

public enum  CucumberProperties {
    INSTANCE;
    private List<Tag> tags = new ArrayList<>();

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
