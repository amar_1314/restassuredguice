package reporting;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public enum ReportProperties {
    INSTANCE;
    //    private String reportFolder = "output/" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH-mm-ss"));
    private String reportFolder = "output/" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    private String reportPath = reportFolder + "/report" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH-mm-ss")) + ".html";
    private String individualReportPath;
    private String extentXServerUrl;
    private String projectName = "default";
    private String documentTitle;
    private String reportName;

    private String klovServerUrl = "http//localhost:5689";
    private String klovProjectName = "Regression Suite";
    private String klovReportName = LocalDateTime.now().format(DateTimeFormatter.ofPattern("MM-dd-yyyy-hh:mm a")) + ".html";
    private String mongodbHost = "localhost";
    private int mongodbPort = 27017;
    private String mongodbDatabase;
    private String mongodbUsername;
    private String mongodbPassword;

    public String getIndividualReportPath() {
        return individualReportPath;
    }

    public void setIndividualReportPath(String individualReportPath) {
        this.individualReportPath = individualReportPath;
    }

    public String getKlovServerUrl() {
        return klovServerUrl;
    }

    public void setKlovServerUrl(String klovServerUrl) {
        this.klovServerUrl = klovServerUrl;
    }

    public String getKlovProjectName() {
        return klovProjectName;
    }

    public void setKlovProjectName(String klovProjectName) {
        this.klovProjectName = klovProjectName;
    }

    public String getKlovReportName() {
        return klovReportName;
    }

    public void setKlovReportName(String klovReportName) {
        this.klovReportName = klovReportName;
    }

    public String getMongodbHost() {
        return mongodbHost;
    }

    public void setMongodbHost(String mongodbHost) {
        this.mongodbHost = mongodbHost;
    }

    public int getMongodbPort() {
        return mongodbPort;
    }

    public void setMongodbPort(int mongodbPort) {
        this.mongodbPort = mongodbPort;
    }

    public String getMongodbDatabase() {
        return mongodbDatabase;
    }

    public void setMongodbDatabase(String mongodbDatabase) {
        this.mongodbDatabase = mongodbDatabase;
    }

    public String getMongodbUsername() {
        return mongodbUsername;
    }

    public void setMongodbUsername(String mongodbUsername) {
        this.mongodbUsername = mongodbUsername;
    }

    public String getMongodbPassword() {
        return mongodbPassword;
    }

    public void setMongodbPassword(String mongodbPassword) {
        this.mongodbPassword = mongodbPassword;
    }

    public String getReportPath() {
        return reportPath;
    }

    public String getExtentXServerUrl() {
        return extentXServerUrl;
    }

    public void setExtentXServerUrl(String extentXServerUrl) {
        this.extentXServerUrl = extentXServerUrl;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDocumentTitle() {
        return documentTitle;
    }

    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportFolder() {
        return this.reportFolder;
    }
}