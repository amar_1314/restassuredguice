package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.Nullable;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@ScenarioScoped
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Lead {

    @Id
    private int leadId;
    private int rawLeadId;
    private Integer parentLeadId;
    private int contactId;
    private String partnerLeadId;
}
