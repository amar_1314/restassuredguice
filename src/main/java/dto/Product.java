package dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Data
@ScenarioScoped
@Entity
public class Product {

    @Id
    private int productId;
    private boolean isOutOfStock;
    private int isActive;
    private int isVisible;
    private int productGroupId;
    private String productName;
    @Transient
    private int productQuantity;
    private double retailCost;
    @JsonProperty("sku")
    private String skuInternal;
    @Transient
    private double totalCost;
}
