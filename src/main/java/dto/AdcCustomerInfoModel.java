package dto;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

@Data
@ScenarioScoped
public class AdcCustomerInfoModel {
    private int customerIdField;
    private String loginNameField;
}
