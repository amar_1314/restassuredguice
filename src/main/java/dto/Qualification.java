package dto;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@Data
@ScenarioScoped
public class Qualification {

    @Id
    private int qualificationId;
    @Transient
    private ContractType[] contractTypes;
    private int leadId;
    private int addressId;
    private int creditScore;
}
