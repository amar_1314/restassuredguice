package dto;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

@ScenarioScoped
@Data
public class AccountInfo {
    private LeadRawInfo leadRawInfo;
    private Lead lead;
    private Account account;
    private Contact personalInfo;
    private Quote quote;
    private Address premisesAddress;
}
