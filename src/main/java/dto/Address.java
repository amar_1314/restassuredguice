package dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.time.LocalDateTime;

@Data
@ScenarioScoped
@Entity
public class Address {

    @Id
    @JsonIgnore
    private int addressId;
    private String line1;
    private String line2;
    private String city;
    private String state;
    private String postalCode;
    @JsonIgnore
    private LocalDateTime dateCreated;
    @JsonIgnore
    private int isDeleted;
    @JsonIgnore
    private String attention;
    @Transient
    @JsonIgnore
    private int addressType;
    @JsonIgnore
    @Transient
    private String country;

}
