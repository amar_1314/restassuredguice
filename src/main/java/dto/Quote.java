package dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Data
@ScenarioScoped
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Quote {

    @Id
    private int quoteId;
    private double contractDiscount;
    private String contractType;
    private int contractTypeID;
    private double discount;
    @Transient
    private List<Product> items = new ArrayList<>();
    private int leadID;
    private String planCode;
    @Transient
    private int planID;
    @Transient
    private String quoteDisplayName;
    @Transient
    private double rmr;
    @JsonAlias("RFTPExtensionDays")
    private int rftpTerm;
    private String shippingCode;
    private double shippingCost;
    private int shippingTypeID;
    @Transient
    private String shippingTypeName;
    private double subtotal;
    @Transient
    private double subtotalMinusDiscounts;
    private double tax;
    @Transient
    private double total;
    @Transient
    private boolean discountIsBelowMax;
    @Transient
    private boolean discountIsValid;
    @Transient
    private boolean isQuoteSaved;
    private double shippingDiscount;

}
