package dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

@Data
@ScenarioScoped
public class NewAddress {

    @JsonAlias("address1")
    private String line1;
    @JsonAlias("address2")
    private String line2 = "";
    private String city;
    private String postalCode;
    private String state;
}
