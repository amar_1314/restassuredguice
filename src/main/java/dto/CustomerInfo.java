package dto;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

@Data
@ScenarioScoped
public class CustomerInfo {
    private AdcCustomerInfoModel adcInfo;
}
