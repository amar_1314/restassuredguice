package dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.time.LocalDateTime;

@Data
@Entity
@ScenarioScoped
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact {

    @Id
    private Integer contactId;
    private String firstName;
    private String lastName;
    private LocalDateTime dateCreated;
    private String phone;
    private String altPhone;
    private String email;
    private String altEmail;
    private Integer addressId;
    private int isResidential;
    private String companyName;
    @JsonIgnore
    @Transient
    private Address address;
}
