package dto;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
@ScenarioScoped
@Data
public class ContractType {

    @Id
    private int contractTypeId;
    private String contractType;
    private int term;
    private double cost;
    private int isActive;
    private String description;
    @Transient
    private double discount;
}
