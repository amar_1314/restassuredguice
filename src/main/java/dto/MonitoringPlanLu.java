package dto;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@ScenarioScoped
@Data
@Entity
public class MonitoringPlanLu {

    @Id
    private int monitoringPlanId;
    private String planName;
    private String planCode;
    private double rmr;
    private int adcid;
    private int isActive;
}
