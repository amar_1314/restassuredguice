package dto;

import cucumber.runtime.java.guice.ScenarioScoped;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Arrays;

@ScenarioScoped
@Entity
public class ShippingTypeLu {

    @Id
    private int shippingTypeId;
    private String name;
    private double additionCost;
    private double systemCost;
    private int fulfillmentPriority;


    public int getShippingTypeId() {
        return shippingTypeId;
    }

    public void setShippingTypeId(int shippingTypeId) {
        this.shippingTypeId = shippingTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAdditionCost() {
        return additionCost;
    }

    public void setAdditionCost(double additionCost) {
        this.additionCost = additionCost;
    }

    public double getSystemCost() {
        return systemCost;
    }

    public void setSystemCost(double systemCost) {
        this.systemCost = systemCost;
    }

    public int getFulfillmentPriority() {
        return fulfillmentPriority;
    }

    public void setFulfillmentPriority(int fulfillmentPriority) {
        this.fulfillmentPriority = fulfillmentPriority;
    }

    public String getShippingCode() {

        return Arrays.stream(ShippingCode.values()).filter(shippingCode -> shippingCode.getName().equals(getName())).findFirst().orElseThrow().getCode();
    }

    private enum ShippingCode {
        Ground("Ground", "Gr"),
        Overnight("Overnight", "Ov"),
        TwoDay("2-Day", "2-D");

        private String code;
        private String name;

        ShippingCode(String name, String code) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }

}
