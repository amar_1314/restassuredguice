package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@ScenarioScoped
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "Order")
public class OrderModel {

    @Id
    private int orderId;
    private int accountId;
    private Integer quoteId;
    private LocalDate computedShipByDate;
    private Double subtotalPrice;
    private Double discountCouponPrice;
    private Double discountContractPrice;
    private Double shippingPrice;
    private Double subtotalPriceWithTax;
    private Double taxTotal;
    private Double totalWithTax;
}
