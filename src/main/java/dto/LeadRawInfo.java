package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@ScenarioScoped
@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LeadRawInfo {

    @Id
    private int rawLeadId;
    private String phone;
    private String email;
    private String firstName;
    private String lastName;

}
