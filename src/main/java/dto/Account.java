package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@ScenarioScoped
@Data
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account {

    @Id
    private Integer accountId;
    private Integer leadId;
    private Integer userCustomerId;
    private Integer shippingContactId;
    private Integer premisesContactId;
    private Integer monitoringContactId;
}
