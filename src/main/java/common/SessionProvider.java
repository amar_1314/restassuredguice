package common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class SessionProvider {

    private final SessionFactory sessionFactory;

    @Inject
    public SessionProvider(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public <T> ConditionBuilder selectFrom(Class<T> tClass) {
        Session session = sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(tClass);
        Root<T> root = query.from(tClass);
        return new SessionBuilder(session, builder, query, root);
    }

    public interface ConditionBuilder {
        ConditionalOperation where(String columnName);

        QueryBuilder buildQuery();
    }

    public interface ConditionalOperation {
        ConditionBuilder isEqualTo(Object value);

        ConditionBuilder isNotEqualTo(Object value);

        ConditionBuilder in(Object... listOfObjects);

        ConditionBuilder isNull();

        ConditionBuilder isNotNull();
    }

    public interface QueryBuilder {
        <T> T get(Class<T> tClass);

        <T> List<T> getList(Class<T> tClass);

        <T> Stream getStream(Class<T> tClass);
    }


    private class SessionBuilder implements ConditionBuilder, ConditionalOperation, QueryBuilder {

        private final CriteriaBuilder builder;
        private final CriteriaQuery query;
        private final Root root;
        private final Session session;
        private final ObjectMapper mapper;
        private List<Predicate> predicateList;
        private String columnName;

        @Inject
        SessionBuilder(Session session, CriteriaBuilder builder, CriteriaQuery query, Root root) {
            this.session = session;
            this.builder = builder;
            this.query = query;
            this.root = root;
            predicateList = new ArrayList<>();
            mapper = new ObjectMapper();
        }

        @Override
        public ConditionalOperation where(String columnName) {
            this.columnName = columnName;
            return this;
        }

        @Override
        public QueryBuilder buildQuery() {
            Predicate[] predicates = new Predicate[predicateList.size()];
            for (int i = 0; i < predicateList.size(); i++) {
                predicates[i] = predicateList.get(i);
            }
            query.select(root).where(predicates);
            return this;
        }


        @Override
        public ConditionBuilder isEqualTo(Object value) {
            predicateList.add(builder.equal(root.get(columnName), String.valueOf(value)));
            return this;
        }

        @Override
        public ConditionBuilder isNotEqualTo(Object value) {
            predicateList.add(builder.notEqual(root.get(columnName), String.valueOf(value)));
            return this;
        }

        @Override
        public ConditionBuilder in(Object... objects) {
            CriteriaBuilder.In inClause = builder.in(root.get(columnName));
            for (Object value : objects) {
                inClause.value(value);
            }
            predicateList.add(inClause);
            return this;
        }

        @Override
        public ConditionBuilder isNull() {
            predicateList.add(builder.isNull(root.get(columnName)));
            return this;
        }

        @Override
        public ConditionBuilder isNotNull() {
            predicateList.add(builder.isNotNull(root.get(columnName)));
            return this;
        }

        @Override
        public <T> T get(Class<T> tClass) {
            return mapper.convertValue(session.createQuery(query).getSingleResult(), mapper.getTypeFactory().constructType(tClass));
        }

        @Override
        public <T> List<T> getList(Class<T> tClass) {
            return mapper.convertValue(session.createQuery(query).getResultList(), mapper.getTypeFactory().constructCollectionType(List.class, tClass));
        }

        @Override
        public <T> Stream getStream(Class<T> tClass) {
            return session.createQuery(query).getResultStream();
        }


    }
}
