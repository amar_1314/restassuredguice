package common;

import org.assertj.core.api.AbstractStandardSoftAssertions;
import org.assertj.core.api.SoftAssertionError;

import java.util.List;

public class Assertions extends AbstractStandardSoftAssertions {

//    public static void assertSoftly(Consumer<SoftAssertions> softly) {
//        SoftAssertions assertions = new SoftAssertions();
//        softly.accept(assertions);
//        assertions.assertAll();
//    }

    public void assertAll() {
        List<Throwable> errors = this.errorsCollected();
        if (!errors.isEmpty()) {
            throw new SoftAssertionError(org.assertj.core.api.Assertions.extractProperty("message", String.class).from(errors));
        }
    }

}
