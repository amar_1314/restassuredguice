package common;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import setup.DriverType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApplicationProperties {

    public CpProperties cp;
    public WbProperties wb;
    public DriverType browser;
    public WeborderProperties weborder;
    public String environment;
    public User adcUser;
    public String creditCardNumber;

    public class CpProperties {
        @JsonAlias({"qaUrl", "uatUrl"})
        public String url;
        public String cookieKey;
        public User defaultUser;

        public class User {
            public String username;
            public String password;
        }
    }

    public class WbProperties {

        public String url;
        public User defaultUser;
        public User identityUser;
        public String partnerLeadId;
        public TestAccount testAccount;
        public String identityClientId;
        public String identityClientSecret;
        public String identityAzureUrl;

        public class User {
            public String username;
            public String password;
        }

        public class TestAccount {
            public String email;
        }
    }

    public class WeborderProperties {
        @JsonAlias({"qaUrl", "uiUrl"})
        public String url;
        public String apiUrl;
        public String identityUrl;
        public User identityUser;
        public String identityClientId;
        public String identityClientSecret;
    }

    public static class User {
        public String username;
        public String password;

    }


}
