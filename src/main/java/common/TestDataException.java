package common;

class TestDataException extends RuntimeException {

    TestDataException(String message) {
        super(message);
    }
}
