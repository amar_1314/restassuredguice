package common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.google.inject.Inject;
import com.google.inject.Provider;

import java.io.File;
import java.io.IOException;

public class ApplicationPropertiesProvider implements Provider<ApplicationProperties> {
    private final ObjectMapper mapper;

    @Inject
    public ApplicationPropertiesProvider(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public ApplicationProperties get() {
        File settingsFile = new File("appSettings.json");

        if (!settingsFile.exists()) {
            throw new TestDataException("App settings json file not found.");
        }

        try {
            ApplicationProperties applicationProperties = mapper.readValue(settingsFile, ApplicationProperties.class);
            ObjectReader updater = mapper.readerForUpdating(applicationProperties);
            if (applicationProperties.environment.toUpperCase().equals("QA")) {
                File qaSettingsFile = new File("appSettings.qa.json");
                if (!qaSettingsFile.exists()) {
                    throw new TestDataException("Qa app settings file not found");
                }
                return updater.readValue(qaSettingsFile);
            } else if (applicationProperties.environment.toUpperCase().equals("UAT")) {
                File uatSettingsFile = new File("appSettings.uat.json");
                if (!uatSettingsFile.exists()) {
                    throw new TestDataException("Uat app settings file not found");
                }
                return updater.readValue(uatSettingsFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new TestDataException("Unable to map setting file content.");
        }

        return null;
    }
}
