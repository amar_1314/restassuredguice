package common;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryProvider implements Provider<SessionFactory> {

    private final ApplicationProperties applicationProperties;
    private final System.Logger logger;

    @Inject
    public SessionFactoryProvider(ApplicationProperties applicationProperties, System.Logger logger) {
        this.applicationProperties = applicationProperties;
        this.logger = logger;
    }

    @Override
    public SessionFactory get() {
        logger.log(System.Logger.Level.INFO, ">>>>>>>>>>>>>> Loading hibernate file hibernate." + applicationProperties.environment + ".cfg.xml");
        return new Configuration()
                .configure("hibernate.cfg.xml")
                .configure("hibernate." + applicationProperties.environment + ".cfg.xml")
                .buildSessionFactory();
    }
}
