package setup;

import com.jayway.restassured.specification.RequestSpecification;
import cucumber.runtime.java.guice.ScenarioScoped;

@ScenarioScoped
public class RestSpec {

    private RequestSpecification weborderRequestSpecification;
    private RequestSpecification cpRequestSpecification;
    private RequestSpecification wbRequestSpecification;

    public RequestSpecification getWbRequestSpecification() {
        return wbRequestSpecification;
    }

    void setWbRequestSpecification(RequestSpecification wbRequestSpecification) {
        this.wbRequestSpecification = wbRequestSpecification;
    }

    public RequestSpecification getCpRequestSpecification() {
        return cpRequestSpecification;
    }

    void setCpRequestSpecification(RequestSpecification cpRequestSpecification) {
        this.cpRequestSpecification = cpRequestSpecification;
    }

    public RequestSpecification getWeborderRequestSpecification() {
        return weborderRequestSpecification;
    }

    void setWeborderRequestSpecification(RequestSpecification weborderRequestSpecification) {
        this.weborderRequestSpecification = weborderRequestSpecification;
    }
}
