package setup;

import com.google.inject.Binder;
import com.google.inject.Module;
import service.ecom.AccountService;
import service.ecom.AccountServiceImpl;
import service.ecom.QualificationService;
import service.ecom.QualificationServiceImpl;
import service.ecom.QuoteService;
import service.ecom.QuoteServiceImpl;
import service.workbench.LeadService;
import service.workbench.LeadServiceImpl;
import service.workbench.OrderService;
import service.workbench.OrderServiceImpl;

public class ServiceModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(LeadService.class).to(LeadServiceImpl.class);
        binder.bind(service.ecom.LeadService.class).to(service.ecom.LeadServiceImpl.class);
        binder.bind(OrderService.class).to(OrderServiceImpl.class);
        binder.bind(QuoteService.class).to(QuoteServiceImpl.class);
        binder.bind(QualificationService.class).to(QualificationServiceImpl.class);
        binder.bind(AccountService.class).to(AccountServiceImpl.class);
        binder.bind(service.ecom.OrderService.class).to(service.ecom.OrderServiceImpl.class);
    }
}
