package setup;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

@ScenarioScoped
@Data
public class TestType {
    private boolean ui;
}
