package setup;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum DriverType {
    @JsonProperty("chrome")
    CHROME,
    FIREFOX,
    IE,
    APITEST
}
