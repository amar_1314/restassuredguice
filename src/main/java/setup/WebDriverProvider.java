package setup;

import com.google.inject.Inject;
import com.google.inject.Provider;
import common.ApplicationProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class WebDriverProvider implements Provider<WebDriver> {
    private DriverType driverType;
    private WebDriver driver = null;
    private final TestType testType;

    @Inject
    public WebDriverProvider(ApplicationProperties properties, TestType testType) {
        driverType = properties.browser;
        this.testType = testType;
    }

    public WebDriver get() {
        if(!testType.isUi()){
            return new HtmlUnitDriver();
        }
        switch (driverType) {
            case CHROME: {
                System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
                driver = new ChromeDriver();
                break;
            }
            case FIREFOX:
                break;
            case IE:
                break;
            default:
                driver = new HtmlUnitDriver();
                break;
        }
        driver.manage().window().maximize();
        return driver;
    }
}
