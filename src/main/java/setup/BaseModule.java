package setup;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Singleton;
import common.ApplicationProperties;
import common.ApplicationPropertiesProvider;
import common.SessionFactoryProvider;
import cucumber.api.guice.CucumberScopes;
import logging.SystemLoggerProvider;
import org.hibernate.SessionFactory;

public class BaseModule implements Module {


    @Override
    public void configure(Binder binder) {
        binder.bind(System.Logger.class).toProvider(SystemLoggerProvider.class).in(Singleton.class);
        binder.bind(ApplicationProperties.class).toProvider(ApplicationPropertiesProvider.class).in(Singleton.class);
        binder.bind(ObjectMapper.class)
                .toInstance(new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
                        .configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true)
                        .registerModule(new Jdk8Module())
                        .registerModule(new JavaTimeModule())
                        .findAndRegisterModules());
        binder.bind(SessionFactory.class).toProvider(SessionFactoryProvider.class).in(CucumberScopes.SCENARIO);
    }
}
