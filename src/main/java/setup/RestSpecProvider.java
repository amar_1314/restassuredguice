package setup;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.http.ContentType;
import common.ApplicationProperties;
import lombok.Data;

import javax.json.Json;

import static com.jayway.restassured.RestAssured.config;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.config.ObjectMapperConfig.objectMapperConfig;

public class RestSpecProvider implements Provider<RestSpec> {
    private static String CP_BASE_URL = null;
    private static String ECOM_API_URL = null;
    private static String WB_BASE_URL = null;
    private final ApplicationProperties properties;
    private final ObjectMapper mapper;

    @Inject
    public RestSpecProvider(ApplicationProperties properties, ObjectMapper mapper) {
        this.properties = properties;
        CP_BASE_URL = properties.cp.url;
        WB_BASE_URL = properties.wb.url;
        ECOM_API_URL = properties.weborder.apiUrl;
        this.mapper = mapper;
    }

    @Override
    public RestSpec get() {
        var restSpecifications = new RestSpec();
        var config = config()
                .objectMapperConfig(objectMapperConfig()
                        .jackson2ObjectMapperFactory((cls, charset) -> mapper));
        restSpecifications.setWeborderRequestSpecification(new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setConfig(config)
                .addHeader("Authorization", "bearer " + getWeborderToken())
                .setBaseUri(ECOM_API_URL)
//                .addFilter(new RequestLoggingFilter())
//                .addFilter(new ResponseLoggingFilter())
                .build());

        restSpecifications.setCpRequestSpecification(new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setConfig(config)
                .addHeader("Authorization", "bearer " + getCpToken())
                .setBaseUri(CP_BASE_URL)
//                .addFilter(new RequestLoggingFilter())
//                .addFilter(new ResponseLoggingFilter())
                .build());

        restSpecifications.setWbRequestSpecification(new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setConfig(config)
                .addHeader("Authorization", "bearer " + getWbToken())
                .setBaseUri(WB_BASE_URL)
//                .addFilter(new RequestLoggingFilter())
//                .addFilter(new ResponseLoggingFilter())
                .build());
        return restSpecifications;

    }

    private String getWbToken() {
        return given().header("Content-Type", "application/x-www-form-urlencoded")
                .formParam("grant_type", "password")
                .formParam("scope", "api introspection permissions")
                .formParam("username", properties.wb.identityUser.username)
                .formParam("password", properties.wb.identityUser.password)
                .formParam("client_id", properties.wb.identityClientId)
                .formParam("client_secret", properties.wb.identityClientSecret)
                .request()
                .post(properties.wb.identityAzureUrl)
                .then()
                .statusCode(200)
                .extract()
                .as(Token.class).getToken();
    }

    private String getWeborderToken() {
        return given().header("Content-Type", "application/x-www-form-urlencoded")
                .formParam("grant_type", "password")
                .formParam("scope", "api introspection permissions")
                .formParam("username", properties.weborder.identityUser.username)
                .formParam("password", properties.weborder.identityUser.password)
                .formParam("client_id", properties.weborder.identityClientId)
                .formParam("client_secret", properties.weborder.identityClientSecret)
                .request()
                .post(properties.weborder.identityUrl)
                .then()
                .statusCode(200)
                .extract()
                .as(Token.class).getToken();
    }

    private String getCpToken() {
        return given()
                .contentType(ContentType.JSON)
                .body(Json.createObjectBuilder()
                        .add("Username", properties.cp.defaultUser.username)
                        .add("Password", properties.cp.defaultUser.password)
                        .add("RememberMe", false)
                        .build().toString())
                .when()
                .post(CP_BASE_URL + "api/Login/token")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .asString().replaceAll("\"", "");
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Token {
        @JsonAlias("access_token")
        private String token;
    }
}
