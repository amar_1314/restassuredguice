package setup;

import com.google.inject.Binder;
import com.google.inject.Module;
import cucumber.api.guice.CucumberScopes;

public class ApiModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(RestSpec.class).toProvider(RestSpecProvider.class).in(CucumberScopes.SCENARIO);
    }
}
