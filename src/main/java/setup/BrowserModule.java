package setup;

import com.google.inject.Binder;
import com.google.inject.Module;
import cucumber.api.guice.CucumberScopes;
import org.openqa.selenium.WebDriver;

public class BrowserModule implements Module {
    @Override
    public void configure(Binder binder) {
        binder.bind(WebDriver.class).toProvider(WebDriverProvider.class).in(CucumberScopes.SCENARIO);
    }
}
