package logging;

import com.google.inject.Provider;

public class SystemLoggerProvider extends System.LoggerFinder implements Provider<System.Logger> {

    @Override
    public System.Logger get() {
        return new ConsoleLogger();
    }

    @Override
    public System.Logger getLogger(String name, Module module) {
        return new ConsoleLogger();
    }
}
