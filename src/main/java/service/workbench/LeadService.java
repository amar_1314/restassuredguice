package service.workbench;

import api_request_resources.LeadRequest;
import dto.Lead;

public interface LeadService {

    Lead postLead(LeadRequest leadRequest);
}
