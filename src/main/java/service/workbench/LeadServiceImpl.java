package service.workbench;

import api_request_resources.LeadRequest;
import com.google.inject.Inject;
import dto.Lead;
import service.workbench.LeadService;
import setup.RestSpec;

import static com.jayway.restassured.RestAssured.given;

public class LeadServiceImpl implements LeadService {

    private final RestSpec restSpec;


    @Inject
    public LeadServiceImpl(RestSpec restSpec) {
        this.restSpec = restSpec;
    }


    @Override
    public Lead postLead(LeadRequest leadRequest) {
        return given().spec(restSpec.getWbRequestSpecification())
                .body(leadRequest)
                .when()
                .post("/api/Lead/Create")
                .then()
                .statusCode(200)
                .extract()
                .as(Lead.class);
    }
}
