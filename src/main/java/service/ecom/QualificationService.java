package service.ecom;

import dto.Qualification;

public interface QualificationService {
    Qualification postQualification();

    void prepareQualificationRequest(boolean withCreditOverride, boolean firstnameForValidCreditScore);
}
