package service.ecom;

import api_request_resources.EcomLeadRequest;
import api_request_resources.EcomQualificationRequest;
import com.google.inject.Inject;
import dto.Address;
import dto.Qualification;
import setup.RestSpec;

import java.time.LocalDateTime;

import static com.jayway.restassured.RestAssured.given;

public class QualificationServiceImpl implements QualificationService {

    private final EcomQualificationRequest ecomQualificationRequest;
    private final EcomLeadRequest ecomLeadRequest;
    private final RestSpec restSpec;


    @Inject
    public QualificationServiceImpl(EcomQualificationRequest ecomQualificationRequest, EcomLeadRequest ecomLeadRequest, RestSpec restSpec) {
        this.ecomQualificationRequest = ecomQualificationRequest;
        this.ecomLeadRequest = ecomLeadRequest;
        this.restSpec = restSpec;
    }


    @Override
    public Qualification postQualification() {

        Qualification qualification = given().spec(restSpec.getWeborderRequestSpecification())
                .body(ecomQualificationRequest)
                .when()
                .post("/v1/leads/" + ecomLeadRequest.getPartnerLeadId() + "/qualifications")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .as(Qualification.class);
        ecomQualificationRequest.setQualification(qualification);
        return qualification;
    }

    @Override
    public void prepareQualificationRequest(boolean withCreditOverride, boolean firstnameForValidCreditScore) {
        Address address = new Address();
        address.setCountry("US");
        address.setLine1(ecomLeadRequest.getLine1());
        address.setLine2(ecomLeadRequest.getLine2());
        address.setCity(ecomLeadRequest.getCity());
        address.setState(ecomLeadRequest.getState());
        address.setPostalCode(ecomLeadRequest.getPostalCode());
        address.setAddressType(1);
        ecomQualificationRequest.setAddress(address);
        if(firstnameForValidCreditScore){
            ecomQualificationRequest.setFirstName(ecomLeadRequest.getFirstName());
        }else {
            ecomQualificationRequest.setFirstName("NotFPTest");
        }
        ecomQualificationRequest.setLastName(ecomLeadRequest.getLastName());
        ecomQualificationRequest.setDateOfBirth(String.valueOf(LocalDateTime.now().minusYears(20)));
        ecomQualificationRequest.setOwnerOfProperty(true);
        ecomQualificationRequest.setPhoneNumber(ecomLeadRequest.getPhone());
        ecomQualificationRequest.setIsCreditOverride(String.valueOf(withCreditOverride));
    }
}
