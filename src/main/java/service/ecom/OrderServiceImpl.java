package service.ecom;

import api_request_resources.CreditCardInfo;
import api_request_resources.EcomAccountRequest;
import api_request_resources.EcomOrderRequest;
import api_request_resources.EcomQuoteRequest;
import com.google.inject.Inject;
import common.ApplicationProperties;
import dto.OrderModel;
import org.junit.Assert;
import setup.RestSpec;

import java.time.LocalDateTime;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;

public class OrderServiceImpl implements OrderService {

    private final EcomQuoteRequest ecomQuoteRequest;
    private final EcomOrderRequest ecomOrderRequest;
    private final EcomAccountRequest ecomAccountRequest;
    private final ApplicationProperties properties;
    private final RestSpec restSpec;


    @Inject
    public OrderServiceImpl(EcomQuoteRequest ecomQuoteRequest, EcomOrderRequest ecomOrderRequest, EcomAccountRequest ecomAccountRequest, ApplicationProperties properties, RestSpec restSpec) {
        this.ecomQuoteRequest = ecomQuoteRequest;
        this.ecomOrderRequest = ecomOrderRequest;
        this.ecomAccountRequest = ecomAccountRequest;
        this.properties = properties;
        this.restSpec = restSpec;
    }

    @Override
    public OrderModel postOrder() {
        prepareOrderRequest();
        OrderModel order = given().spec(restSpec.getWeborderRequestSpecification())
                .body(ecomOrderRequest)
                .when()
                .post("/v1/orders")
                .then()
                .statusCode(200)
                .extract()
                .as(OrderModel.class);
        ecomOrderRequest.setOrderId(order.getOrderId());
        Assert.assertNotEquals("Order created with id 0", 0, order.getOrderId());
        return order;
    }

    private void prepareOrderRequest() {
        ecomOrderRequest.setPartnerLeadId(ecomQuoteRequest.getPartnerLeadId());
        ecomOrderRequest.setQuoteId(ecomQuoteRequest.getQuoteId());
        ecomOrderRequest.setShippingContactId(ecomAccountRequest.getShippingContactId());
        ecomOrderRequest.setPaymentContactId(ecomAccountRequest.getPremisesContactId());
        CreditCardInfo creditCardInfo = new CreditCardInfo();
        creditCardInfo.setCardnumber(properties.creditCardNumber);
        creditCardInfo.setExpirationDate(LocalDateTime.now().plusYears(5));
        creditCardInfo.setCvv("555");
        ecomOrderRequest.setUpfrontPaymentInformation(creditCardInfo);
        ecomOrderRequest.setRecurringPaymentInformationCredit(creditCardInfo);
        ecomOrderRequest.setTax(ecomOrderRequest.getTax());
        ecomOrderRequest.setTotal(ecomQuoteRequest.getTotal());
        ecomOrderRequest.setOrderAttributes(Map.of("EcomAutomation", "true"));
    }
}
