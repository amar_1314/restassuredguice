package service.ecom;

import dto.Account;

public interface AccountService {
    Account postAccount();
}
