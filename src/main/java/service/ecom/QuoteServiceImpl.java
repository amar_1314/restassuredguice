package service.ecom;

import api_request_resources.EcomLeadRequest;
import api_request_resources.EcomQuoteRequest;
import com.google.inject.Inject;
import dto.Quote;
import org.junit.Assert;
import setup.RestSpec;

import static com.jayway.restassured.RestAssured.given;

public class QuoteServiceImpl implements QuoteService {

    private final RestSpec restSpec;
    private final EcomQuoteRequest ecomQuoteRequest;
    private final EcomLeadRequest ecomLeadRequest;


    @Inject
    public QuoteServiceImpl(RestSpec restSpec, EcomQuoteRequest ecomQuoteRequest, EcomLeadRequest ecomLeadRequest) {
        this.restSpec = restSpec;
        this.ecomQuoteRequest = ecomQuoteRequest;
        this.ecomLeadRequest = ecomLeadRequest;
    }

    @Override
    public Quote postQuote() {
        ecomQuoteRequest.setPartnerLeadId(ecomLeadRequest.getPartnerLeadId());
        ecomQuoteRequest.setCoupons(new EcomQuoteRequest.Coupon[0]);
        Quote quote = given().spec(restSpec.getWeborderRequestSpecification())
                .body(ecomQuoteRequest)
                .when()
                .post("/v1/quotes")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .as(Quote.class);
        Assert.assertNotEquals("Quote generated with id 0", 0, quote.getQuoteId());
        return quote;
    }
}
