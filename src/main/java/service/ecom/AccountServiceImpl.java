package service.ecom;

import api_request_resources.AlarmInformation;
import api_request_resources.EcomAccountRequest;
import api_request_resources.EcomLeadRequest;
import api_request_resources.EcomQuoteRequest;
import api_request_resources.MonitoringPlan;
import com.google.inject.Inject;
import common.SessionProvider;
import dto.Account;
import dto.Lead;
import org.junit.Assert;
import setup.RestSpec;

import static com.jayway.restassured.RestAssured.given;

public class AccountServiceImpl implements AccountService {

    private final EcomAccountRequest ecomAccountRequest;
    private final EcomQuoteRequest ecomQuoteRequest;
    private final EcomLeadRequest ecomLeadRequest;
    private final SessionProvider sessionProvider;
    private final RestSpec restSpec;


    @Inject
    public AccountServiceImpl(EcomAccountRequest ecomAccountRequest, EcomQuoteRequest ecomQuoteRequest, EcomLeadRequest ecomLeadRequest, SessionProvider sessionProvider, RestSpec restSpec) {
        this.ecomAccountRequest = ecomAccountRequest;
        this.ecomQuoteRequest = ecomQuoteRequest;
        this.ecomLeadRequest = ecomLeadRequest;
        this.sessionProvider = sessionProvider;
        this.restSpec = restSpec;
    }

    @Override
    public Account postAccount() {

        prepareAccountRequest();
        Account account = given().spec(restSpec.getWeborderRequestSpecification())
                .body(ecomAccountRequest)
                .when()
                .post("/v1/accounts")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .as(Account.class);
        ecomAccountRequest.setAccountId(account.getAccountId());
        Assert.assertNotEquals("Account created with id 0",0.0,account.getAccountId());
        return account;
    }

    private void prepareAccountRequest() {
        Lead lead = sessionProvider.selectFrom(Lead.class).where("leadId").isEqualTo(ecomLeadRequest.getLeadId()).buildQuery().get(Lead.class);
        ecomAccountRequest.setQuoteId(ecomQuoteRequest.getQuoteId());
        AlarmInformation alarmInformation = new AlarmInformation();
        alarmInformation.setPassCode("test");
        if (notACameraOnlyPlanAccount()) {
            alarmInformation.setInitialUserCode("1314");
        }
        ecomAccountRequest.setAlarmInformation(alarmInformation);
        ecomAccountRequest.setShippingContactId(lead.getContactId());
        ecomAccountRequest.setPremisesContactId(lead.getContactId());
    }

    private boolean notACameraOnlyPlanAccount() {
        return ecomQuoteRequest.getPlanId() != MonitoringPlan.CO.getMonitoringPlanId();
    }
}
