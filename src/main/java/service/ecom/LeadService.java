package service.ecom;

import dto.Lead;

public interface LeadService {

    void prepareEcomLeadRequest();

    Lead postLead();

}
