package service.ecom;

import dto.OrderModel;

public interface OrderService {

    OrderModel postOrder();
}
