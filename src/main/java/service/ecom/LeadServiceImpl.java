package service.ecom;

import api_request_resources.EcomLeadRequest;
import com.github.javafaker.Faker;
import com.google.inject.Inject;
import common.ApplicationProperties;
import common.SessionProvider;
import dto.CustomerInformationModel;
import dto.Lead;
import helper.Formatter;
import helper.HelperFunctions;
import org.junit.Assert;
import setup.RestSpec;

import java.util.Objects;

import static com.jayway.restassured.RestAssured.given;

public class LeadServiceImpl implements LeadService {

    private final EcomLeadRequest ecomLeadRequest;
    private final ApplicationProperties properties;
    private final Faker faker;
    private final RestSpec restSpec;
    private final SessionProvider sessionProvider;
    private final CustomerInformationModel customerInformationModel;


    @Inject
    public LeadServiceImpl(EcomLeadRequest ecomLeadRequest, ApplicationProperties properties, Faker faker, RestSpec restSpec, SessionProvider sessionProvider, CustomerInformationModel customerInformationModel) {
        this.ecomLeadRequest = ecomLeadRequest;
        this.properties = properties;
        this.faker = faker;
        this.restSpec = restSpec;
        this.sessionProvider = sessionProvider;
        this.customerInformationModel = customerInformationModel;
    }


    @Override
    public void prepareEcomLeadRequest() {
        ecomLeadRequest.setPartnerLeadId(properties.wb.partnerLeadId);
        ecomLeadRequest.setFirstName("FPTest");
        ecomLeadRequest.setLastName(faker.name().lastName());
        ecomLeadRequest.setEmailAddress(Formatter.fpEmail.format(faker.internet().emailAddress()));
        ecomLeadRequest.setSecondaryEmailAddress(Formatter.fpEmail.format(faker.internet().emailAddress()));
        ecomLeadRequest.setPhone(HelperFunctions.getValidPhoneNumber());
        ecomLeadRequest.setSecondaryPhone(HelperFunctions.getValidPhoneNumber());
        ecomLeadRequest.setResidential(true);
        ecomLeadRequest.setLine1(faker.address().streetAddress());
        ecomLeadRequest.setLine2(faker.address().secondaryAddress());
        ecomLeadRequest.setCity("Vienna");
        ecomLeadRequest.setState("VA");
        ecomLeadRequest.setPostalCode("22182");
        customerInformationModel.setFirstName(ecomLeadRequest.getFirstName());
        customerInformationModel.setLastName(ecomLeadRequest.getLastName());
        customerInformationModel.setEmail(ecomLeadRequest.getEmailAddress());
        customerInformationModel.setAltEmail(ecomLeadRequest.getSecondaryEmailAddress());
        customerInformationModel.setPhone(ecomLeadRequest.getPhone());
        customerInformationModel.setAltPhone(ecomLeadRequest.getSecondaryEmailAddress());
    }

    @Override
    public Lead postLead() {
        Objects.requireNonNull(ecomLeadRequest);
        EcomLeadRequest leadRequest = given().spec(restSpec.getWeborderRequestSpecification())
                .body(ecomLeadRequest)
                .when()
                .post("/v1/leads")
                .then()
                .statusCode(201)
                .extract()
                .as(EcomLeadRequest.class);
        ecomLeadRequest.setPartnerLeadId(leadRequest.getPartnerLeadId());
        Lead lead = sessionProvider.
                selectFrom(Lead.class)
                .where("partnerLeadId").isEqualTo(leadRequest.getPartnerLeadId())
                .where("parentLeadId").isNull()
                .buildQuery().get(Lead.class);
        ecomLeadRequest.setLeadId(lead.getLeadId());
        Assert.assertNotEquals("Lead generated with id 0", 0, lead.getLeadId());
        return lead;
    }
}
