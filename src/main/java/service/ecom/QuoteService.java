package service.ecom;

import dto.Quote;

public interface QuoteService {
    Quote postQuote();
}
