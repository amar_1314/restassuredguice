Feature: As a ecomm user I should be able to see difference in response time after caching api calls


  Scenario: Verify if api calls response from same source time has been reduced after caching
    Given I have all api calls to make that can be cached
    When I make api calls for "3" times before cache
    Then I capture response time


  Scenario: Verify if api calls from different sources response time has been reduced after caching
    Given I have all api calls to make that can be cached
    When I make api calls from different sources for "10" times before cache
    Then I capture response time


  Scenario: Verify if api calls from all different sources response time has been reduced after caching
    Given I have all api calls to make that can be cached
    When I make api calls from all different sources for "3" times before cache
    Then I capture response time


  Scenario: Capture time for tax transaction post call
    Given I have tax transaction call url
    When I make api call for tax transaction for "2" times
    Then I capture response time


  Scenario: Test individual url with same user
    Given I have "https://qa-fpws.fpssi.com/v1/ShippingTypes" api url
    When I make api call
    Then I capture response time