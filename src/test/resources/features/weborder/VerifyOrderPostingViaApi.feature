Feature: Verify orders posted via api


  @api
  Scenario: Verify if user is able to post an order with 3 year contract
    Given I have lead info
    When I post ecomm lead via api
    And I set contract term for ecomm quote
      | ThreeYear |
    And I set plan for ecomm quote
      | IM |
    And I set shipping for ecomm quote
      | Ground |
    And I set products for ecomm quote
      | Frontpoint Hub |
    And I post ecomm quote via api
    And I post ecomm qualification via api
    And I post ecomm account via api
    And I post ecomm order via api
    Then I verify ecomm account created via api
    And I verify account is accessible in workbench
    And I verify order details on workbench orders page