Feature: As a frontpoint customer I should be able to submit order through ecomm

  @ui @regression @ecomm @uat @test
  Scenario: Submit order with a package
    Given I am on ecomm home page
    When I click on shop packages button
    And I click on buy now button on 1 package
#    And I select IM plan
    And I click on review order button
    And I click on secure checkout button
    And I enter customer information
    And I enter emergency contact information
    And I enter system location address
    And I select 1 shipping method
    And I click on credit check button
    And I enter date of birth
    And I run credit check
    And I fill in credit card information
    And I sign contract
    And I grab order details on review page
    Then I place order
    And I verify if order has been submitted
    And I verify account is accessible in workbench
    And I verify order details on workbench orders page


  @ui @regression @ecomm @uat
  Scenario: Submit order with a bigger package
    Given I am on ecomm home page
    When I click on shop packages button
    And I click on buy now button on 5 package
    And I click on review order button
    And I click on secure checkout button
    And I enter customer information
    And I enter emergency contact information
    And I enter system location address
    And I select 3 shipping method
    And I click on credit check button
    And I enter date of birth
    And I run credit check
    And I fill in credit card information
    And I sign contract
    And I grab order details on review page
    Then I place order
    And I verify if order has been submitted
    And I verify account is accessible in workbench
    And I verify order details on workbench orders page

  @ui @regression @ecomm @uat @build-your-own
  Scenario: Submit build your own order
    Given I am on ecomm home page
    When I click on build your own button
    And I add products
      | product            | quantity |
      | Door/Window Sensor | 1        |
      | Glass Break Sensor | 3        |
      | Flood Sensor       | 2        |
      | Yard Sign          | 1        |
      | Door Stickers      | 2        |
    And I click on monitoring plan button
    And I click on review order button
    And I click on secure checkout button
    And I enter customer information
    And I enter emergency contact information
    And I enter system location address
    And I select 1 shipping method
    And I click on credit check button
    And I enter date of birth
    And I run credit check
    And I fill in credit card information
    And I sign contract
    And I grab order details on review page
    Then I place order
    And I verify if order has been submitted
    And I verify account is accessible in workbench
    And I verify order details on workbench orders page

  @ui @regression @ecomm @uat @build-your-own
  Scenario: Submit build your own order with more addons
    Given I am on ecomm home page
    When I click on build your own button
    And I add products
      | product                 | quantity |
      | Frontpoint Keypad       | 2        |
      | Door/Window Sensor      | 1        |
      | Garage Door Tilt Sensor | 3        |
      | Glass Break Sensor      | 3        |
      | Flood Sensor            | 2        |
      | Smoke and Heat Sensor   | 2        |
      | Premium Indoor Camera   | 2        |
      | Indoor Camera           | 1        |
      | Garage Door Controller  | 1        |
      | Wireless Light Control  | 1        |
      | Yard Sign               | 1        |
      | Door Stickers           | 2        |
    And I click on monitoring plan button
    And I click on review order button
    And I click on secure checkout button
    And I enter customer information
    And I enter emergency contact information
    And I enter system location address
    And I select 1 shipping method
    And I click on credit check button
    And I enter date of birth
    And I run credit check
    And I fill in credit card information
    And I sign contract
    And I grab order details on review page
    Then I place order
    And I verify if order has been submitted
    And I verify account is accessible in workbench
    And I verify order details on workbench orders page

  @ui @regression @ecomm @uat @build-your-own
  Scenario: Submit build your own order with less addons
    Given I am on ecomm home page
    When I click on build your own button
    And I add products
      | product            | quantity |
      | Door/Window Sensor | 1        |
    And I click on monitoring plan button
    And I click on review order button
    And I click on secure checkout button
    And I enter customer information
    And I enter emergency contact information
    And I enter system location address
    And I select 1 shipping method
    And I click on credit check button
    And I enter date of birth
    And I run credit check
    And I fill in credit card information
    And I sign contract
    And I grab order details on review page
    Then I place order
    And I verify if order has been submitted
    And I verify account is accessible in workbench
    And I verify order details on workbench orders page

  @ui @regression @ecomm @uat
  Scenario: Submit order with a package and addons
    Given I am on ecomm home page
    When I click on shop packages button
    And I click on personalize solution button for package 2
    And I add products
      | product                 | quantity |
      | Garage Door Tilt Sensor | 2        |
      | Indoor Camera           | 1        |
      | Wireless Light Control  | 1        |
    And I click on monitoring plan button
    And I click on review order button
    And I click on secure checkout button
    And I enter customer information
    And I enter emergency contact information
    And I enter system location address
    And I select 2 shipping method
    And I click on credit check button
    And I enter date of birth
    And I run credit check
    And I fill in credit card information
    And I sign contract
    And I grab order details on review page
    Then I place order
    And I verify if order has been submitted
    And I verify account is accessible in workbench
    And I verify order details on workbench orders page

  @ui @regression @ecomm @uat
  Scenario: Submit order with a package and addons that are part of package
    Given I am on ecomm home page
    When I click on shop packages button
    And I click on personalize solution button for package 1
    And I add products
      | product            | quantity |
      | Door/Window Sensor | 1        |
      | Motion Sensor      | 2        |
    And I click on monitoring plan button
    And I click on review order button
    And I click on secure checkout button
    And I enter customer information
    And I enter emergency contact information
    And I enter system location address
    And I select 2 shipping method
    And I click on credit check button
    And I enter date of birth
    And I run credit check
    And I fill in credit card information
    And I sign contract
    And I grab order details on review page
    Then I place order
    And I verify if order has been submitted
    And I verify account is accessible in workbench
    And I verify order details on workbench orders page