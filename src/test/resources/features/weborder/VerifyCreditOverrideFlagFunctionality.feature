Feature: To verify isCreditOverride flag functionality


  @api @regression @uat @ecomm
  Scenario: Verify if credit check is bypassed if isCreditOverride is set to true
    Given I have lead info
    When I post ecomm lead via api
    Then I post ecomm qualification via api with isCreditOverride flag "true" and first name to get valid credit score to "false"
    And I verify if user is qualified for all contract types via api

  @api @regression @uat @ecomm
  Scenario: Verify if credit check is not bypassed if isCreditOverride is set to false
    Given I have lead info
    When I post ecomm lead via api
    Then I post ecomm qualification via api with isCreditOverride flag "false" and first name to get valid credit score to "false"
    And I verify if user is qualified only for one year contract

  @api @regression @uat @ecomm
  Scenario: Verify if credit check is bypassed if isCreditOverride is set to false and first name set to get valid credit score
    Given I have lead info
    When I post ecomm lead via api
    Then I post ecomm qualification via api with isCreditOverride flag "false" and first name to get valid credit score to "true"
    And I verify if user is qualified for all contract types but month to month via api

  @api @regression @uat @ecomm
  Scenario: Verify if credit check is bypassed if creditOverride is set to true and first name set to get valid credit score
    Given I have lead info
    When I post ecomm lead via api
    Then I post ecomm qualification via api with isCreditOverride flag "true" and first name to get valid credit score to "true"
    And I verify if user is qualified for all contract types via api
