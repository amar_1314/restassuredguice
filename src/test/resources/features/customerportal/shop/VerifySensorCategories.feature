Feature: Verify sensor categories

  Scenario: Verify if all sensor categories are displayed
    Given I query for sensor category list
    Then I verify sensor category list

  Scenario: Verify sensor, home automation and cameras count
    Given I query for product list
    Then I verify product count

  @ui
  Scenario: Verify cameras section
    Given I am on customerportal app login page
    When I log into customerportal app as default user
    And I click on MyAccount tab
    And I click on Shop tab
    And I click on Cameras tab
    And I query for product list
    Then I verify cameras displayed
    And I verify cameras cost

