Feature: Verify account info section on account settings page

  Background: Make sure user is on login page
    Given I am on customerportal app login page

    @ui
  Scenario: Verify account info view
    Given I log into customerportal app as default user
    Then I query for account info
    And I verify account info

    @ui @regression
  Scenario: Edit account info and verify save
    Given I log into customerportal app as default user
    Then I query for account info
    And I edit account info with
      | email  | phone  | altPhone |
      | random | random | random   |
    And I verify account info
