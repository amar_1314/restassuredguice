Feature: Moving form page functionality

  Background: Navigate to customer portal app
    Given I am on customerportal app login page

  Scenario: Verify pre-filled premise address in movers kit address section
    Given I log into customerportal app as default user
    And I query for account info
    When I navigate to move portal page
    And I do not have a pending move
    Then I click on start move button
    And I see premise address is pre-filled in the mover's kit address section

