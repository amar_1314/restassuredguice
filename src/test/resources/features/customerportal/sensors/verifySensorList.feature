Feature: As a customer portal user, I should be able to navigate to sensors page and view sensor list

  @api @regression
  Scenario: Verify sensor list from adc and customer portal via api
    Given I query for sensor list from adc
    When I query for sensor list from customer portal
    Then I compare sensor list from adc and customer portal

  @api @regression
  Scenario: Verify sensor categories from adc and customer portal via api
    Given I query for sensor list from adc
    When I query for sensor list from customer portal
    Then I verify device categories on sensors page

  @api @regression
  Scenario: Verify sensor names from adc and customer portal via api
    Given I query for sensor list from adc
    When I query for sensor list from customer portal
    Then I verify device names on sensors page
