Feature: As a sales manager, I should be able to assign callbacks to a sales rep

  @api @regression
  Scenario: Verify if sales manager is able to assign call back to an agent
    Given I post a lead via api
    When I post a call back schedule by assigning it to a sales rep
    Then I verify call back schedule record in CallSchedule table
    And I verify there is only one call back schedule record for the lead generated