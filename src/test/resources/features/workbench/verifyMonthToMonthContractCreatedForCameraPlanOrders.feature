Feature: As a workbench user, I should be able to verify a Month-to-Month contract has been created for camera plan

  @api @regression
  Scenario: Verify Month-to-Month contract in contract type table
    Given I query contractType table
    Then I verify Month-to-Month contract type is returned

  @api @regression
  Scenario: Verify if user is able to place an order with Month-to-Month plan
    Given I post a lead via api
    And I have quote items
      | productName           | productQuantity |
      | Premium Indoor Camera | 2               |
      | Indoor Camera         | 1               |
    And I post quote via api with
      | shippingTypeName | contractType   | planCode |
      | Ground           | Month to month | CO       |
    And I post initial order via api

