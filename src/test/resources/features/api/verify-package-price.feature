Feature: To verify package price

  Scenario: Verify pro package retail price
    Given user have weborder cookie
    When user query for offer
    Then user gets actual package retail price
    Then user calculates expected package retail price with
      | XT Panel              | 1 |
      | Motion Sensor         | 1 |
      | Door or Window Sensor | 3 |
      | Glass Break Sensor    | 1 |
      | Keychain Remote       | 1 |