Feature: CalculatedShipByDate column in the database should be set based on order scheduled date not order created date for relocation orders

  @api @regression
  Scenario Outline: Verify if CalculatedShipByDate is populated in order table based on scheduled date for relocation order
    Given I post relocation request with "<scheduledDate>" date
    And I query db for order info
    Then I verify scheduled date "<scheduledDate>" and calculated ship by date "<calculatedShipByDate>"

    Examples:
      | scheduledDate | calculatedShipByDate |
      | Friday        | Tuesday              |
      | Monday        | Wednesday            |
      | Sunday        | Tuesday              |
      | Saturday      | Tuesday              |
      | Thursday      | Monday               |
    

    @api @regression
    Scenario: Verify if CalculatedShipByDate is populated in order table based on edited scheduled date for relocation order
      Given I post relocation request with "Friday" date
      And I query db for order info
      When I update scheduled date for relocation order created with "Thursday"
      And I query db for order info
      Then I verify scheduled date "Thursday" and calculated ship by date "Monday"

