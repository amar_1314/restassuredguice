Feature: Verify customer information on customer info screen

  @api @wb
  Scenario: Verify personal info on customer info screen
    Given I query db for accountId
    And I query db for personal info with accountId
    And I query api for personal info with accountId
    Then I compare personal info from db with api