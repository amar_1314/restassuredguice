package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"reporting.ExtentReportPlugin:"},
        features = {"classpath:features"},
        glue = {"classpath:steps", "classpath:setup"},
        tags = {"@regression", "~@uat"},
        monochrome = true
)
public class RegressionTest {

}
