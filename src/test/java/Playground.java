import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//import reporting.TestClass;

public class Playground {


    @Test
    public void oneTest() {

//        var x = Stream.iterate(new int[]{0, 1}, n -> new int[]{n[1], n[0] + n[1]})
//
//                .limit(10).map(n -> n[0])
//                .takeWhile(n-> n<=5)
//                .collect(Collectors.toList());
//        System.out.println(x);
        var str = "asdab";
        var charMap = new LinkedHashMap<Character, Integer>();

        for (char strChar : str.toCharArray()) {
            if (charMap.keySet().contains(strChar)) {
                charMap.put(strChar, charMap.get(strChar) + 1);
            } else {
                charMap.put(strChar, 1);
            }
        }


        charMap.keySet().stream().filter(k -> charMap.get(k) == 1)
                .findFirst()
                .ifPresentOrElse(c -> System.out.println(c), () -> System.out.println("no element"));


        List<Item> items = Arrays.asList(
                new Item("apple", 10),
                new Item("banana", 20),
                new Item("orang", 10),
                new Item("watermelon", 10),
                new Item("papaya", 20),
                new Item("apple", 10),
                new Item("banana", 10),
                new Item("apple", 20)
        );
        var x = items.stream().collect(Collectors.groupingBy(i -> i.name, Collectors.summingInt(i -> i.qty)));
        x.entrySet().forEach(e -> System.out.println(e.getKey() + "," + e.getValue()));
    }

    @Test
    public void testtest() {
        String testStr  = "adfhakasdh asdfasdf asdfasd testk";


        Map<Character, Integer> result = testStr.chars().boxed()
                .collect(Collectors.toMap(k-> Character.valueOf((char)k.intValue()), v-> v=1, Integer::sum));

        for(Character character: testStr.toCharArray()){
            if(result.get(character)==1){
                System.out.println(character);
                break;
            }
        }


        System.out.println(result);










    }

    @Test
    public void testTestTest() throws Throwable {

        String[][] inputArray = {{"B", "B", "W"},
                {"W", "W", "W"},
                {"W", "W", "W"},
                {"B", "B", "B"}};

        String updatingEle = inputArray[2][2];
        String requiredUpdate = "G";



        Arrays.stream(inputArray)
                .map(e-> String.join(" ",e))
                .map(e-> {
                    if(e.contains(updatingEle)){
                        return e.replaceAll(updatingEle, requiredUpdate);

                    }else {
                        return e;
                    }
                })
                .forEach(System.out::println);

    }

    @Test
    public void timeTest() {

    }

    public class Item {
        private String name;
        private int qty;
        private BigDecimal price;
        public Item(String name, int qty) {
            this.name = name;
            this.qty = qty;
        }

    }

    private class ArrayComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            if (o1 < o2) {
                return 1;
            } else if (o1.equals(o2)) {
                return 0;
            }
            return -1;
        }
    }
}
