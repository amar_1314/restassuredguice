package models;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;
import pages.customerportal.sensors.SensorListPage;

@Data
@ScenarioScoped
public class DeviceList {
    private SensorListPage.DeviceList adcDeviceList;
    private SensorListPage.DeviceList cpDeviceList;
}
