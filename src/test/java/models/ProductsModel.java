package models;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

@Data
@ScenarioScoped
public class ProductsModel {
    private String product;
    private int quantity;
}
