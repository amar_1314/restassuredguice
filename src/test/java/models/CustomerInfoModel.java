package models;

import cucumber.runtime.java.guice.ScenarioScoped;
import dto.CustomerInformationModel;

@ScenarioScoped
public class CustomerInfoModel {
    private Address billingAddress;
    private Address premisesAddress;
    private Address shippingAddress;
    private CustomerInformationModel customerInformation;

    public Address getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Address getPremisesAddress() {
        return premisesAddress;
    }

    public void setPremisesAddress(Address premisesAddress) {
        this.premisesAddress = premisesAddress;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public CustomerInformationModel getCustomerInformation() {
        return customerInformation;
    }

    public void setCustomerInformation(CustomerInformationModel customerInformation) {
        this.customerInformation = customerInformation;
    }
}
