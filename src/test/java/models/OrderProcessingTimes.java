package models;

import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;

import java.time.LocalDateTime;

@ScenarioScoped
@Data
public class OrderProcessingTimes {
    private LocalDateTime submissionTime;
    private LocalDateTime processedTime;
}
