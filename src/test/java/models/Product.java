package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Data;
import lombok.Getter;
import shop.Shop;

import java.util.List;

@Data
@ScenarioScoped
public class Product {
    private String catalogId;
    private String categoryId;
    private String name;
    private String url;
    @JsonProperty("isBuyable")
    private boolean buyable;
    @JsonProperty("isActive")
    private boolean active;
    @Getter(lazy = true)
    private final Shop.Category sensorCategory = getCategory();

    private Shop.Category getCategory() {
        assert url != null;
        return List.of(Shop.Category.values())
                .stream().filter(category -> category.getCategory()
                        .equals(url.split("/")[1]))
                .findFirst()
                .orElseThrow();
    }
}
