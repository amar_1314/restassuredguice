package pages;

import com.google.inject.Inject;
import common.ApplicationProperties;
import helper.Ui;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage {

    private final ApplicationProperties properties;
    private final Ui ui;

    private By logoutButton = By.cssSelector("[ng-click='$ctrl.logout()']");
    private By loginButton = By.cssSelector("[type='submit']");
    private By username = By.cssSelector("#form-login [name='login-username']");
    private By password = By.cssSelector("#form-login [name='login-password']");

    @Inject
    public LoginPage(ApplicationProperties properties, Ui ui) {
        this.properties = properties;
        this.ui = ui;
    }

    public void loginAsDefaultUser() {
        ui.getWait(30)
                .until(ExpectedConditions.elementToBeClickable(loginButton));

        ui.findElement(username).scroll()
                .sendKeys(properties.cp.defaultUser.username);
        ui.findElement(password).scroll()
                .sendKeys(properties.cp.defaultUser.password);
        ui.findElement(loginButton).scroll().click();
        ui.getWait(30)
                .until(ExpectedConditions.elementToBeClickable(logoutButton));
    }
}
