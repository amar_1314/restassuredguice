package pages;

import com.github.javafaker.Faker;
import com.google.inject.Inject;
import common.Assertions;
import helper.Formatter;
import helper.Regex;
import helper.Ui;
import models.AccountInfo;
import dto.CustomerInformationModel;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.stream.Stream;

public class AccountSettingsPage {
    private final AccountInfo accountInfo;
    private final Assertions assertions;
    private final Ui ui;
    private final Faker faker;

    private By accountInfoBy = By.cssSelector("#account form .form-control-static");
    private By editAccountInfoButtonBy = By.cssSelector("#account [type='button'] .fa-pencil");
    private By emailEditBy = By.id("emailInput");
    private By phoneEditBy = By.id("primaryPhoneInput");
    private By altPhoneEditBy = By.id("secondaryPhoneInput");
    private By saveButtonBy = By.cssSelector("#account button[type='submit']");


    @Inject
    public AccountSettingsPage(AccountInfo accountInfo, Assertions assertions, Ui ui, Faker faker) {
        this.accountInfo = accountInfo;
        this.assertions = assertions;
        this.ui = ui;
        this.faker = faker;
    }

    public void verityAccountInfo() {
        var expectedCustomerInfo = accountInfo.customerInformation;
        ui.waitForCpLoading();
        var accountInfoElements = ui.getWait(40)
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(accountInfoBy));

        assertions.assertThat(accountInfoElements.get(0).getText())
                .as("Name")
                .isEqualTo(expectedCustomerInfo.getFirstName() + " " + expectedCustomerInfo.getLastName());
        assertions.assertThat(accountInfoElements.get(1).getText())
                .as("Email")
                .isEqualTo(expectedCustomerInfo.getEmail());
        assertions.assertThat(Formatter.tenDigitNumber.format(accountInfoElements.get(2).getText()))
                .as("Primary phone")
                .isEqualTo(Formatter.tenDigitNumber.format(expectedCustomerInfo.getPhone()));
        assertions.assertThat(Formatter.tenDigitNumber.format(accountInfoElements.get(3).getText()))
                .as("Secondary phone")
                .isEqualTo(Formatter.tenDigitNumber.format(expectedCustomerInfo.getAltPhone()));
        assertions.assertAll();
    }

    public void editAccountInfoWith(CustomerInformationModel customerInformation) {
        ui.getWait(20).until(ExpectedConditions.elementToBeClickable(editAccountInfoButtonBy)).click();
        var info = generateData(customerInformation);
        accountInfo.customerInformation.setEmail(info.getEmail());
        accountInfo.customerInformation.setPhone(info.getPhone());
        accountInfo.customerInformation.setAltPhone(info.getAltPhone());
        ui.findElement(emailEditBy).scroll().clear();
        ui.findElement(emailEditBy).scroll().sendKeys(info.getEmail());
        Ui.sleep();
        ui.findElement(phoneEditBy).scroll().clear();
        ui.findElement(phoneEditBy).scroll().sendKeys(info.getPhone());
        Ui.sleep();
        ui.findElement(altPhoneEditBy).scroll().clear();
        ui.findElement(altPhoneEditBy).scroll().sendKeys(info.getAltPhone());
        Ui.sleep();
    }

    public void saveAccountInfo() {
        ui.getWait(20)
                .until(ExpectedConditions.elementToBeClickable(saveButtonBy))
                .click();
    }

    private CustomerInformationModel generateData(CustomerInformationModel customerInformation) {
        if (customerInformation.getEmail().equalsIgnoreCase("random")) {
            customerInformation.setEmail(Formatter.fpEmail.format(faker.internet().emailAddress()));
        }
        if (customerInformation.getPhone().equalsIgnoreCase("random")) {
            customerInformation.setPhone(
                    getPhoneNumber()
            );
        }
        if (customerInformation.getAltPhone().equalsIgnoreCase("random")) {
            customerInformation.setAltPhone(
                    getPhoneNumber()
            );
        }
        return customerInformation;
    }

    private String getPhoneNumber() {

        return Formatter.tenDigitNumber.format(
                Stream.iterate(0, i -> i < 10, i -> i++).map(i -> faker.phoneNumber().cellPhone())
                        .filter(p -> p.matches(Regex.validPhoneNumber)).findFirst().orElse(null)
        );
    }
}
