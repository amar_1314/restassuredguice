package pages.ecom;

import api_request_resources.ContractType;
import api_request_resources.EcomQuoteRequest;
import api_request_resources.MonitoringPlan;
import api_request_resources.ShippingType;
import com.google.inject.Inject;
import common.SessionProvider;
import dto.Product;
import dto.Product_;
import dto.Quote;
import service.ecom.QuoteService;

import java.util.List;
import java.util.stream.Collectors;

public class QuotePage {

    private final EcomQuoteRequest ecomQuoteRequest;
    private final SessionProvider sessionProvider;
    private final QuoteService quoteService;


    @Inject
    public QuotePage(EcomQuoteRequest ecomQuoteRequest, SessionProvider sessionProvider, QuoteService quoteService) {
        this.ecomQuoteRequest = ecomQuoteRequest;
        this.sessionProvider = sessionProvider;
        this.quoteService = quoteService;
    }


    public void setContractType(ContractType contractType) {
        ecomQuoteRequest.setContractTypeID(contractType.getContractTypeId());
    }

    public void setMonitoringPlan(MonitoringPlan monitoringPlan) {
        ecomQuoteRequest.setPlanId(monitoringPlan.getMonitoringPlanId());
    }

    public void setShippingType(ShippingType shippingType) {
        ecomQuoteRequest.setShipping(new EcomQuoteRequest.Shipping());
        ecomQuoteRequest.getShipping().setShippingTypeId(shippingType.getShippingTypeId());
    }

    public void setProductSkuList(List<String> productSkuList) {

        List<Product> productList = productSkuList.stream().map(sku -> sessionProvider.selectFrom(Product.class)
                .where(Product_.SKU_INTERNAL).isEqualTo(sku)
                .where(Product_.IS_ACTIVE).isEqualTo(1)
                .where(Product_.IS_VISIBLE).isEqualTo(1)
                .buildQuery()
                .get(Product.class)).collect(Collectors.toList());

        EcomQuoteRequest.Item[] items = new EcomQuoteRequest.Item[productList.size()];

        for (int i = 0; i < productList.size(); i++) {
            items[i] = new EcomQuoteRequest.Item();
            items[i].setProductId(productList.get(i).getProductId());
            items[i].setProductSku(productList.get(i).getSkuInternal());
            items[i].setProductCost(productList.get(i).getRetailCost());
        }

        ecomQuoteRequest.setItems(items);
    }

    public Quote postQuote() {

        return quoteService.postQuote();

    }

}
