package pages.ecom;

import com.google.inject.Inject;
import dto.OrderModel;
import service.ecom.OrderService;

public class OrderPage {

    private final OrderService orderService;


    @Inject
    public OrderPage(OrderService orderService) {
        this.orderService = orderService;
    }


    public OrderModel postOrder() {
        return orderService.postOrder();
    }
}
