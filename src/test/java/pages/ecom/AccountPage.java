package pages.ecom;

import api_request_resources.EcomAccountRequest;
import api_request_resources.EcomLeadRequest;
import api_request_resources.EcomOrderRequest;
import api_request_resources.EcomQuoteRequest;
import com.google.inject.Inject;
import common.Assertions;
import common.SessionProvider;
import dto.Account;
import dto.Account_;
import dto.Lead_;
import dto.OrderModel;
import dto.OrderModel_;
import dto.Quote;
import dto.Quote_;
import service.ecom.AccountService;

public class AccountPage {

    private final AccountService accountService;
    private final EcomAccountRequest ecomAccountRequest;
    private final EcomOrderRequest ecomOrderRequest;
    private final EcomQuoteRequest ecomQuoteRequest;
    private final EcomLeadRequest ecomLeadRequest;
    private final SessionProvider sessionProvider;
    private final Assertions assertions;


    @Inject
    public AccountPage(AccountService accountService, EcomAccountRequest ecomAccountRequest, EcomOrderRequest ecomOrderRequest, EcomQuoteRequest ecomQuoteRequest, EcomLeadRequest ecomLeadRequest, SessionProvider sessionProvider, Assertions assertions) {
        this.accountService = accountService;
        this.ecomAccountRequest = ecomAccountRequest;
        this.ecomOrderRequest = ecomOrderRequest;
        this.ecomQuoteRequest = ecomQuoteRequest;
        this.ecomLeadRequest = ecomLeadRequest;
        this.sessionProvider = sessionProvider;
        this.assertions = assertions;
    }


    public Account postAccount() {
        return accountService.postAccount();
    }

    public void VerifyAccountCreatedForInitialOrder() {
        int leadId = ecomLeadRequest.getLeadId();
        Quote quote = sessionProvider
                .selectFrom(Quote.class)
                .where(Quote_.LEAD_ID)
                .isEqualTo(leadId)
                .buildQuery()
                .get(Quote.class);
        Account account = sessionProvider
                .selectFrom(Account.class)
                .where(Account_.LEAD_ID)
                .isEqualTo(leadId)
                .buildQuery()
                .get(Account.class);
        OrderModel order = sessionProvider
                .selectFrom(OrderModel.class)
                .where(Lead_.LEAD_ID)
                .isEqualTo(leadId)
                .buildQuery()
                .get(OrderModel.class);

        assertions.assertThat(quote.getQuoteId())
                .as("Quote id")
                .isEqualTo(ecomQuoteRequest.getQuoteId());
        assertions.assertThat(account.getAccountId())
                .as("Account id")
                .isEqualTo(ecomAccountRequest.getAccountId());
        assertions.assertThat(order.getOrderId())
                .as("Order id")
                .isEqualTo(ecomOrderRequest.getOrderId());
        assertions.assertAll();
    }
}
