package pages.ecom;

import api_request_resources.EcomQualificationRequest;
import com.google.inject.Inject;
import common.SessionProvider;
import dto.ContractType;
import dto.ContractType_;
import dto.Qualification;
import service.ecom.QualificationService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class QualificationPage {

    private final QualificationService qualificationService;
    private final SessionProvider sessionProvider;
    private final EcomQualificationRequest ecomQualificationRequest;


    @Inject
    public QualificationPage(QualificationService qualificationService, SessionProvider sessionProvider, EcomQualificationRequest ecomQualificationRequest) {
        this.qualificationService = qualificationService;
        this.sessionProvider = sessionProvider;
        this.ecomQualificationRequest = ecomQualificationRequest;
    }


    public Qualification postQualification() {
        return qualificationService.postQualification();
    }

    public void prepareEcomQualificationRequest(boolean withCreditOverride, boolean firstnameForValidCreditScore) {
        qualificationService.prepareQualificationRequest(withCreditOverride, firstnameForValidCreditScore);
    }

    public List<ContractType> getContractTypesDb(boolean isCreditOverride) {
        List<ContractType> contractTypeList = sessionProvider
                .selectFrom(ContractType.class)
                .where(ContractType_.IS_ACTIVE)
                .isEqualTo(1)
                .buildQuery()
                .getList(ContractType.class);
        if (!isCreditOverride) {
            return contractTypeList.stream()
                    .filter(contractType -> contractType.getContractTypeId() == 1)
                    .collect(Collectors.toList());
        }

        return contractTypeList;
    }

    public List<ContractType> getContractTypesDb(api_request_resources.ContractType... contractTypes){
        List<Integer> contractTypeIds = Arrays.stream(contractTypes).map(api_request_resources.ContractType::getContractTypeId)
                .collect(Collectors.toList());
        return sessionProvider
                .selectFrom(ContractType.class)
                .where(ContractType_.IS_ACTIVE)
                .isEqualTo(1)
                .where(ContractType_.CONTRACT_TYPE_ID)
                .in(contractTypeIds)
                .buildQuery()
                .getList(ContractType.class);
    }

    public List<ContractType> getNonMonthToMonthContractTypesDb() {

        int monthToMonthContractTypeId = api_request_resources.ContractType.MonthToMonth.getContractTypeId();
        return sessionProvider
                .selectFrom(ContractType.class)
                .where(ContractType_.IS_ACTIVE)
                .isEqualTo(1)
                .where(ContractType_.CONTRACT_TYPE_ID)
                .isNotEqualTo(monthToMonthContractTypeId)
                .buildQuery()
                .getList(ContractType.class);
    }

    public ContractType[] getContractTypesApi() {
        return ecomQualificationRequest.getQualification().getContractTypes();
    }
}
