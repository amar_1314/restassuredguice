package pages.ecom;

import com.google.inject.Inject;
import dto.Lead;
import service.ecom.LeadService;

public class LeadPage {

    private final LeadService leadService;


    @Inject
    public LeadPage(LeadService leadService) {
        this.leadService = leadService;
    }


    public void prepareEcomLeadRequest() {
        leadService.prepareEcomLeadRequest();

    }

    public Lead postLead() {
        return leadService.postLead();
    }
}
