package pages.customerportal.sensors;


import com.fasterxml.jackson.annotation.JsonAlias;
import com.google.inject.Inject;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.xml.XmlPath;
import com.jayway.restassured.response.Response;
import common.ApplicationProperties;
import common.Assertions;
import cucumber.runtime.java.guice.ScenarioScoped;
import dto.AdcCustomerInfoModel;
import dto.CustomerInfo;
import lombok.Data;
import reporting.Reporter;
import setup.RestSpec;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import static com.jayway.restassured.RestAssured.given;

public class SensorListPage {

    private final ApplicationProperties properties;
    private final RestSpec restSpec;
    private final CustomerInfo customerInfo;
    private final System.Logger logger;
    private final Assertions assertions;


    @Inject
    public SensorListPage(ApplicationProperties properties, RestSpec restSpec, CustomerInfo customerInfo, System.Logger logger, Assertions assertions) {
        this.properties = properties;
        this.restSpec = restSpec;
        this.customerInfo = customerInfo;
        this.logger = logger;
        this.assertions = assertions;
    }

    public DeviceList getSensorListFromAdc() {

//        TODO: For now "Unknown" status from ADC is considered "Connected" in CP
        List<String> unusualSensorStatusFromAdc = List.of("Malfunction", "Offline", "Tamper");

        customerInfo.setAdcInfo(given().spec(restSpec.getCpRequestSpecification())
                .when()
                .get("/api/Login/AdcCustomerInfo")
                .then()
                .statusCode(200)
                .extract()
                .as(AdcCustomerInfoModel.class));

        Response response = given().body(getDeviceListRequestBody())
                .contentType("text/xml")
                .when().post("https://alarmadmin.alarm.com/WebServices/CustomerManagement.asmx")
                .then()
                .contentType(ContentType.XML)
                .statusCode(200)
                .extract()
                .response();

        DeviceList deviceList = new DeviceList();
        List<DeviceDetail> deviceDetails = new ArrayList<>();

        List<String> panelDevices = XmlPath.from(response.asString()).getList("Envelope.Body.GetDeviceListResponse.GetDeviceListResult.PanelDevice");

        deviceList.setDeviceCount(panelDevices.size());

        for (int i = 0; i < deviceList.getDeviceCount(); i++) {
            String deviceName = XmlPath.from(response.asString()).get("Envelope.Body.GetDeviceListResponse.GetDeviceListResult.PanelDevice[" + i + "].WebSiteDeviceName");
            logger.log(System.Logger.Level.INFO, "Got device name: " + deviceName);
            int deviceId = Integer.parseInt(XmlPath.from(response.asString()).get("Envelope.Body.GetDeviceListResponse.GetDeviceListResult.PanelDevice[" + i + "].DeviceId"));
            logger.log(System.Logger.Level.INFO, "Got deviceId: " + deviceId);
            int groupId = Integer.parseInt(XmlPath.from(response.asString()).get("Envelope.Body.GetDeviceListResponse.GetDeviceListResult.PanelDevice[" + i + "].Group"));
            logger.log(System.Logger.Level.INFO, "Got groupId: " + groupId);
            List<String> deviceStatusList = XmlPath.from(response.asString()).getList("Envelope.Body.GetDeviceListResponse.GetDeviceListResult.PanelDevice[" + i + "].Status.DeviceStatusEnum");
            String deviceStatus = null;
            for (String status : deviceStatusList) {
                if (unusualSensorStatusFromAdc.contains(status)) {
                    deviceStatus = "Sensor" + status;
                    break;
                } else {
                    deviceStatus = "Connected";
                }
            }
            logger.log(System.Logger.Level.INFO, "Got device status: " + deviceStatus);
            deviceDetails.add(new DeviceDetail(deviceName, deviceStatus, deviceId, groupId));
        }


        deviceList.setDeviceDetails(deviceDetails);
        Reporter.addStepLog("Device list from ADC: \r\nDevice count:" + deviceList.getDeviceCount() + "\r\nDevices: " + deviceList.getDeviceDetails());

        return deviceList;

    }

    public DeviceList getSensorListFromCp() {

        List<CpDeviceList> cpDeviceList = List.of(given().spec(restSpec.getCpRequestSpecification())
                .when()
                .get("/api/sensors")
                .then()
                .statusCode(200)
                .extract()
                .as(CpDeviceList[].class));
        DeviceList deviceList = new DeviceList();
        List<DeviceDetail> deviceDetails = new ArrayList<>();
        deviceList.setDeviceCount(cpDeviceList.size());

        for (int i = 0; i < deviceList.getDeviceCount(); i++) {
            deviceDetails.add(new DeviceDetail(cpDeviceList.get(i).getDeviceName(), cpDeviceList.get(i).getDeviceStatus(), cpDeviceList.get(i).getDeviceId(), cpDeviceList.get(i).getGroupName()));
        }

        deviceList.setDeviceDetails(deviceDetails);

        Reporter.addStepLog("Device list from CP: \r\nDevice count:" + deviceList.getDeviceCount() + "\r\nDevices: " + deviceList.getDeviceDetails());

        return deviceList;

    }

    public DeviceList updateAdcDeviceListWithGroupNames(DeviceList adcDeviceList) {
        adcDeviceList.getDeviceDetails().forEach(
                deviceDetail -> {
                    switch (deviceDetail.getGroupId()) {
                        case 10:
                        case 13:
                        case 17: {
                            deviceDetail.setGroupName("Intrusion");
                            break;
                        }
                        case 26:
                        case 34:
                        case 38:
                        case 29: {
                            deviceDetail.setGroupName("Environmental");
                            break;
                        }
                        case 0: {
                            if (deviceDetail.getDeviceId() != 127) {
                                deviceDetail.setGroupName("Keypad");
                            }
                            break;
                        }
                        default: {
                            Reporter.addStepLog("Skipping group id:" + deviceDetail.getGroupId() + " for sensor: " + deviceDetail.getDeviceName());
                            logger.log(System.Logger.Level.INFO, "Skipping group id:" + deviceDetail.getGroupId() + " for sensor: " + deviceDetail.getDeviceName());
                            break;
                        }

                    }
                }
        );

        return adcDeviceList;

    }

    public void compareDeviceListFromAdcAndCp(models.DeviceList deviceList) {
        assertions.assertThat(deviceList.getCpDeviceList().getDeviceCount()).describedAs("Device count").isEqualTo(deviceList.getAdcDeviceList().getDeviceCount());
        assertions.assertAll();
        IntStream.range(0, deviceList.getAdcDeviceList().getDeviceCount())
                .forEach(iteration -> {
                    DeviceDetail adcDeviceDetail = deviceList.getAdcDeviceList().getDeviceDetails().get(iteration);
                    DeviceDetail cpDeviceDetail = deviceList.getCpDeviceList().getDeviceDetails().get(iteration);
                    assertions.assertThat(cpDeviceDetail.getDeviceId())
                            .as("Asserting deviceId")
                            .isEqualTo(adcDeviceDetail.getDeviceId());
                    assertions.assertThat(cpDeviceDetail.getDeviceName())
                            .as("Asserting device name")
                            .isEqualTo(adcDeviceDetail.getDeviceName());
                    assertions.assertThat(cpDeviceDetail.getGroupName())
                            .as("Asserting group name")
                            .isEqualTo(adcDeviceDetail.getGroupName());
                    assertions.assertThat(cpDeviceDetail.getDeviceStatus())
                            .as("Asserting device status")
                            .isEqualTo(adcDeviceDetail.getDeviceStatus());

                });
        assertions.assertAll();
    }

    public void verifyDeviceCategories(DeviceList adcDeviceList, DeviceList cpDeviceList) {
        int adcIntrusionCount = (int) adcDeviceList.getDeviceDetails().stream()
                .filter(deviceDetail -> deviceDetail.getGroupName().equals("Intrusion")).count();

        int cpIntrusionCount = (int) cpDeviceList.getDeviceDetails().stream()
                .filter(deviceDetail -> deviceDetail.getGroupName().equals("Intrusion")).count();

        int adcEnvironmentalCount = (int) adcDeviceList.getDeviceDetails().stream()
                .filter(deviceDetail -> deviceDetail.getGroupName().equals("Environmental")).count();

        int cpEnvironmentalCount = (int) cpDeviceList.getDeviceDetails().stream()
                .filter(deviceDetail -> deviceDetail.getGroupName().equals("Environmental")).count();

        int adcKeypadCount = (int) adcDeviceList.getDeviceDetails().stream()
                .filter(deviceDetail -> deviceDetail.getGroupName().equals("Keypad")).count();

        int cpKeypadCount = (int) cpDeviceList.getDeviceDetails().stream()
                .filter(deviceDetail -> deviceDetail.getGroupName().equals("Keypad")).count();

        assertions.assertThat(cpIntrusionCount)
                .as("Intrusion devices count")
                .isEqualTo(adcIntrusionCount);
        assertions.assertThat(cpEnvironmentalCount)
                .as("Environmental devices count")
                .isEqualTo(adcEnvironmentalCount);
        assertions.assertThat(cpKeypadCount)
                .as("Keypad devices count")
                .isEqualTo(adcKeypadCount);

        assertions.assertAll();
    }

    public void verifyDeviceNames(DeviceList adcDeviceList, DeviceList cpDeviceList) {
        IntStream.range(0, adcDeviceList.getDeviceDetails().size())
                .forEach(iteration -> assertions.assertThat(cpDeviceList.getDeviceDetails().get(iteration).getDeviceName())
                        .as("Comparing device name")
                        .isEqualTo(adcDeviceList.getDeviceDetails().get(iteration).getDeviceName()));
        assertions.assertAll();
    }

    private String getDeviceListRequestBody() {
        String body = null;
        try {
            body = new String(Files.readAllBytes(Paths.get("src/test/data/request_body/getDeviceListRequest.xml")))
                    .replaceAll("AdcUsername", properties.adcUser.username).
                            replaceAll("AdcPassword", properties.adcUser.password)
                    .replaceAll("AdcCustomerId", String.valueOf(customerInfo.getAdcInfo().getCustomerIdField()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Objects.requireNonNull(body);
    }

    @Data
    private static class CpDeviceList {
        @JsonAlias("adcNumber")
        private int deviceId;
        private String groupName;
        @JsonAlias("name")
        private String deviceName;
        private int setupOrder;
        @JsonAlias("sku")
        private String skuName;
        @JsonAlias("status")
        private String deviceStatus;
    }

    @Data
    @ScenarioScoped
    public class DeviceList {
        private int deviceCount;
        private List<DeviceDetail> deviceDetails;
    }

    @Data
    @ScenarioScoped
    private class DeviceDetail {
        private String deviceName;
        private String deviceStatus;
        private int deviceId;
        private String groupName;
        private int groupId;

        DeviceDetail(String deviceName, String deviceStatus, int deviceId, int groupId) {
            this.deviceName = deviceName;
            this.deviceStatus = deviceStatus;
            this.deviceId = deviceId;
            this.groupId = groupId;
        }

        DeviceDetail(String deviceName, String deviceStatus, int deviceId, String groupName) {
            this.deviceName = deviceName;
            this.deviceStatus = deviceStatus;
            this.deviceId = deviceId;
            this.groupName = groupName;
        }
    }
}
