package pages.customerportal.moveportal;

import com.google.inject.Inject;
import com.jayway.restassured.response.Response;
import dto.OrderModel;
import lombok.Data;
import models.Address;
import setup.RestSpec;

import java.time.LocalDate;

import static com.jayway.restassured.RestAssured.given;

public class MoversKitDeliveryPage {

    private final RestSpec restSpec;


    @Inject
    public MoversKitDeliveryPage(RestSpec restSpec) {
        this.restSpec = restSpec;
    }

    public OrderModel postRelocationRequest(Address movingAddress, LocalDate movingDate, Address deliveryAddress, LocalDate deliveryDate) {

        return given().spec(restSpec.getCpRequestSpecification())
                .body(getRelocationModel(movingAddress, movingDate, deliveryAddress, deliveryDate))
                .when()
                .post("/api/relocation/info")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .as(OrderModel.class);
    }

    public void cancelAllPendingRelocationOrders() {

        for (int i = 0; isPendingRelocationExists() && i < 5; i++) {
            given().spec(restSpec.getCpRequestSpecification())
                    .when()
                    .delete("/api/relocation/cancel");
        }
    }

    public OrderModel updateRelocationRequest(Address movingAddress, LocalDate movingDate, Address deliveryAddress, LocalDate deliveryDate, int orderId) {

        return given().spec(restSpec.getCpRequestSpecification())
                .body(getRelocationModel(movingAddress, movingDate, deliveryAddress, deliveryDate, orderId))
                .when()
                .put("/api/relocation/info")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .as(OrderModel.class);

    }

    private boolean isPendingRelocationExists() {
        Response relocationResponse = given().spec(restSpec.getCpRequestSpecification())
                .when()
                .get("/api/relocation/info")
                .then()
                .extract()
                .response();
        if (relocationResponse.getStatusCode() == 204) {
            return false;
        }
        return relocationResponse.as(OrderModel.class).getOrderId() > 0;
    }

    private RelocationModel getRelocationModel(Address movingAddress, LocalDate movingDate, Address deliveryAddress, LocalDate deliveryDate, int orderId) {
        RelocationModel relocationModel = getRelocationBody(movingAddress, movingDate, deliveryAddress, deliveryDate);

        relocationModel.setOrderId(orderId);

        return relocationModel;
    }

    private RelocationModel getRelocationModel(Address movingAddress, LocalDate movingDate, Address deliveryAddress, LocalDate deliveryDate) {
        return getRelocationBody(movingAddress, movingDate, deliveryAddress, deliveryDate);
    }

    private RelocationModel getRelocationBody(Address movingAddress, LocalDate movingDate, Address deliveryAddress, LocalDate deliveryDate) {
        RelocationModel relocationModel = new RelocationModel();
        AddressModel addressModel = new AddressModel();
        relocationModel.setLine1(movingAddress.getAddress1());
        relocationModel.setLine2(movingAddress.getAddress2());
        relocationModel.setCity(movingAddress.getCity());
        relocationModel.setState(movingAddress.getState());
        relocationModel.setPostalCode(movingAddress.getPostalCode());
        relocationModel.setDateRelocating(movingDate.toString());

        addressModel.setLine1(deliveryAddress.getAddress1());
        addressModel.setLine2(deliveryAddress.getAddress2());
        addressModel.setCity(deliveryAddress.getCity());
        addressModel.setState(deliveryAddress.getState());
        addressModel.setPostalCode(deliveryAddress.getPostalCode());

        relocationModel.setShippingAddress(addressModel);

        relocationModel.setOrderProcessingDate(deliveryDate.toString());

        return relocationModel;
    }

    @Data
    private class RelocationModel {
        private String line1;
        private String line2;
        private String city;
        private String state;
        private String postalCode;
        private String dateRelocating;
        private AddressModel shippingAddress;
        private String orderProcessingDate;
        private int orderId;

    }

    @Data
    private class AddressModel {
        private String line1;
        private String line2;
        private String city;
        private String state;
        private String postalCode;
    }
}
