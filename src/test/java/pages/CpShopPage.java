package pages;

import com.google.inject.Inject;
import shop.Shop;

import java.util.Set;

public class CpShopPage {

    private final Shop shop;

    @Inject
    public CpShopPage(Shop shop) {
        this.shop = shop;
    }


    public void queryForSensorCategoryList() {
        assert !shop.getActiveProductsMap().keySet().isEmpty();
    }

    public Set<Shop.Category> getSensorCategories() {
        return shop.getActiveProductsMap().keySet();
    }

    public Set<Shop.Category> getActualSensorCategories() {
        return Set.of(Shop.Category.SENSORS, Shop.Category.ACCESSORIES, Shop.Category.CAMERAS, Shop.Category.HOMEAUTOMATION, Shop.Category.PRODUCT);
    }

    public void queryForProductList() {
        assert !shop.getProductList().isEmpty();
    }

    public int getSensorCount() {
        return Shop.Category.SENSORS.getList().size();
    }

    public int getCameraCount(){
        return shop.getActiveProductsMap().get(Shop.Category.CAMERAS).size();
    }

    public int getHomeAutomationCount(){
        return shop.getActiveProductsMap().get(Shop.Category.HOMEAUTOMATION).size();
    }
}
