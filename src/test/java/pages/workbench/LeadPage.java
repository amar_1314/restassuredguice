package pages.workbench;

import api_request_resources.LeadRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.google.inject.Inject;
import common.SessionProvider;
import dto.AccountInfo;
import dto.Address;
import dto.Address_;
import dto.Contact;
import dto.Contact_;
import dto.Lead;
import dto.LeadRawInfo;
import dto.Lead_;
import dto.NewAddress;
import helper.HelperFunctions;
import org.jetbrains.annotations.NotNull;
import service.workbench.LeadService;
import setup.RestSpec;

import java.util.Map;

import static com.jayway.restassured.RestAssured.given;

public class LeadPage {


    private final RestSpec restSpec;
    private final SessionProvider sessionProvider;
    private final ObjectMapper mapper;
    private final LeadService leadService;
    private final AccountInfo accountInfo;
    private final System.Logger logger;


    @Inject
    public LeadPage(RestSpec restSpec, SessionProvider sessionProvider, ObjectMapper mapper, LeadService leadService, AccountInfo accountInfo, System.Logger logger) {
        this.restSpec = restSpec;
        this.sessionProvider = sessionProvider;
        this.mapper = mapper;
        this.leadService = leadService;
        this.accountInfo = accountInfo;
        this.logger = logger;
    }

    public double getTaxPercentage(Address address) {
        return given().spec(restSpec.getWbRequestSpecification())
                .parameters(getTaxRequestParameters(address))
                .when()
                .get("/api/sales-tax-rates")
                .then()
                .statusCode(200)
                .extract()
                .jsonPath()
                .getDouble("Data.Total");

    }

    public Lead postLead() {
        return leadService.postLead(buildLeadRequestBody());
    }


    public Address getPremisesAddress(int leadId) {

        int contactId = sessionProvider.selectFrom(Lead.class).where(Lead_.LEAD_ID).isEqualTo(leadId).buildQuery().get(Lead.class).getContactId();

        int addressId = sessionProvider.selectFrom(Contact.class).where(Contact_.CONTACT_ID).isEqualTo(contactId).buildQuery().get(Contact.class).getAddressId();

        return sessionProvider.selectFrom(Address.class).where(Address_.ADDRESS_ID).isEqualTo(addressId).buildQuery().get(Address.class);

    }

    private LeadRequest buildLeadRequestBody() {

        LeadRequest body = new LeadRequest();
        NewAddress address = new NewAddress();
        Faker faker = new Faker();
        address.setLine1(faker.address().streetAddress());
        address.setLine2(faker.address().secondaryAddress());
        address.setCity("Vienna");
        address.setState("VA");
        address.setPostalCode("22182");

        body.setAddress(address);
        LeadRequest.Compliance compliance = new LeadRequest.Compliance();
        compliance.setTcpaPermissionGranted(true);
        body.setCompliance(compliance);
        body.setEmail(faker.internet().emailAddress());
        body.setFirstName(faker.name().firstName());
        body.setLastName(faker.name().lastName());
        body.setPhone(HelperFunctions.getValidPhoneNumber());

        updateLeadInformation(body);
        logger.log(System.Logger.Level.INFO, ">>>>> Posting lead with request body: " + body.toString());

        return body;
    }

    private void updateLeadInformation(@NotNull LeadRequest body) {
        LeadRawInfo leadRawInfo = new LeadRawInfo();
        leadRawInfo.setFirstName(body.getFirstName());
        leadRawInfo.setLastName(body.getLastName());
        leadRawInfo.setPhone(body.getPhone());
        accountInfo.setLeadRawInfo(leadRawInfo);
    }

    private Map<String, ?> getTaxRequestParameters(Address address) {
        return mapper.convertValue(address, mapper.getTypeFactory().constructMapLikeType(Map.class, String.class, Object.class));
    }
}
