package pages.workbench;

import com.google.inject.Inject;
import service.workbench.OrderService;

public class InitialOrderPage {

    private final OrderService orderService;

    @Inject
    public InitialOrderPage(OrderService orderService) {
        this.orderService = orderService;
    }


    public void postInitialOrder() {

    }
}
