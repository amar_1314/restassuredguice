package pages.workbench;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import common.SessionProvider;
import dto.AccountInfo;
import dto.ContractType;
import dto.ContractType_;
import dto.MonitoringPlanLu;
import dto.MonitoringPlanLu_;
import dto.Product;
import dto.Quote;
import dto.ShippingTypeLu;
import dto.ShippingTypeLu_;
import org.jetbrains.annotations.NotNull;
import setup.RestSpec;

import java.util.List;

import static com.jayway.restassured.RestAssured.given;

public class QuotePage {
    private final Quote quote;
    private final AccountInfo accountInfo;
    private final SessionProvider sessionProvider;
    private final RestSpec restSpec;
    private final ObjectMapper mapper;


    @Inject
    public QuotePage(Quote quote, AccountInfo accountInfo, SessionProvider sessionProvider, RestSpec restSpec, ObjectMapper mapper) {
        this.quote = quote;
        this.accountInfo = accountInfo;
        this.sessionProvider = sessionProvider;
        this.restSpec = restSpec;
        this.mapper = mapper;
    }

    public void setQuoteItems(List<Product> quoteItems) {

        List<Product> productListDb = sessionProvider.selectFrom(Product.class).buildQuery().getList(Product.class);
        quoteItems.forEach(product -> quote.getItems().add(productListDb.stream().filter(p -> p.getProductName().equals(product.getProductName()))
                .peek(p -> p.setProductQuantity(product.getProductQuantity()))
                .findFirst().orElseThrow()));
    }

    public void postQuote(Quote quote) {
        Quote quoteRequestBody = getQuoteRequestBody(quote);
        Object response = given().spec(restSpec.getWbRequestSpecification())
                .body(quoteRequestBody)
                .when()
                .post("/api/sales/quotes/")
                .then()
                .statusCode(200)
                .extract()
                .jsonPath()
                .get("Data.SalesQuote");
        Quote quoteResponse = mapper.convertValue(response, Quote.class);
        accountInfo.setQuote(quoteResponse);
    }

    private Quote getQuoteRequestBody(@NotNull Quote quoteData) {
        ContractType contractTypeDb = sessionProvider.selectFrom(ContractType.class).where(ContractType_.DESCRIPTION).isEqualTo(quoteData.getContractType()).buildQuery().get(ContractType.class);
        MonitoringPlanLu monitoringPlanLuDb = sessionProvider.selectFrom(MonitoringPlanLu.class).where(MonitoringPlanLu_.PLAN_CODE).isEqualTo(quoteData.getPlanCode()).buildQuery().get(MonitoringPlanLu.class);
        ShippingTypeLu shippingTypeLu = sessionProvider.selectFrom(ShippingTypeLu.class).where(ShippingTypeLu_.NAME).isEqualTo(quoteData.getShippingTypeName()).buildQuery().get(ShippingTypeLu.class);
        quote.setContractTypeID(contractTypeDb.getContractTypeId());
        quote.setContractType(contractTypeDb.getContractType());
        quote.setLeadID(accountInfo.getLead().getLeadId());
        quote.setPlanCode(monitoringPlanLuDb.getPlanCode());
        quote.setPlanID(monitoringPlanLuDb.getMonitoringPlanId());
        quote.setQuoteDisplayName("[New Quote]");
        quote.setRmr(monitoringPlanLuDb.getRmr());
        quote.setRftpTerm(30);
        quote.setShippingCode(shippingTypeLu.getShippingCode());
        quote.setShippingCost(shippingTypeLu.getSystemCost());
        quote.setShippingDiscount(shippingTypeLu.getSystemCost());
        quote.setShippingTypeID(shippingTypeLu.getShippingTypeId());
        quote.setShippingTypeName(shippingTypeLu.getName());
        quote.setSubtotal(99.99);
        quote.setSubtotalMinusDiscounts(99.99);
        quote.setTax(6);
        quote.setTotal(105.99);
        quote.setDiscountIsBelowMax(true);
        quote.setDiscountIsValid(true);
        return quote;
    }


}
