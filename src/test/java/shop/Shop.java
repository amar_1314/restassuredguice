package shop;

import com.google.inject.Inject;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.Setter;
import models.Product;
import setup.RestSpec;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.jayway.restassured.RestAssured.given;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@ScenarioScoped
public class Shop {

    private static Function<Category, List<Product>> getActiveSensorList;
    private final RestSpec restSpec;
    @Getter
    @Setter
    private List<Product> activeProducts;
    @Getter
    @Setter
    private List<Product> productList;
    @Getter
    @Setter
    private List<Product> buyableProducts;
    @Getter
    @Setter
    private Map<Category, List<Product>> activeProductsMap;
    @Getter
    @Setter
    private Map<Category, List<Product>> buyableProductMap;


    @Inject
    public Shop(RestSpec restSpec) {
        this.restSpec = restSpec;
        queryForProducts();
        getActiveSensorList = this::activeProducts;
    }

    private List<Product> activeProducts(Category category) {

        setActiveProducts(getProductList()
                .stream()
                .filter(product -> product.getSensorCategory() == category)
                .filter(Product::isActive)
                .collect(Collectors.toList()));
        return getActiveProducts();
    }

    private void queryForProducts() {
        setProductList(
                List.of(given().spec(restSpec.getCpRequestSpecification())
                        .when()
                        .get("/shop/store1/storefrontapi/allProducts")
                        .then()
                        .statusCode(200)
                        .extract()
                        .response()
                        .as(Product[].class))
        );

        setActiveProducts(
                getProductList()
                        .stream()
                        .filter(Product::isActive)
                        .collect(Collectors.toList())
        );

        setBuyableProducts(
                getActiveProducts()
                        .stream()
                        .filter(Product::isBuyable)
                        .collect(Collectors.toList())
        );

        setActiveProductsMap(
                getActiveProducts()
                        .stream()
                        .collect(groupingBy(Product::getSensorCategory, toList()))
        );

        setBuyableProductMap(
                getBuyableProducts()
                        .stream()
                        .collect(groupingBy(Product::getSensorCategory, toList()))
        );
    }

    public enum Category {
        CAMERAS("cameras") {
            public List<Product> getList() {
                return getActiveSensorList.apply(Category.CAMERAS);
            }
        },
        SENSORS("sensors") {
            public List<Product> getList() {
                return getActiveSensorList.apply(Category.SENSORS);
            }
        },
        HOMEAUTOMATION("home-automation") {
            public List<Product> getList() {
                return getActiveSensorList.apply(Category.HOMEAUTOMATION);
            }
        },
        ACCESSORIES("accessories") {
            public List<Product> getList() {
                return getActiveSensorList.apply(Category.ACCESSORIES);
            }
        },
        PRODUCT("product") {
            public List<Product> getList() {
                return getActiveSensorList.apply(Category.PRODUCT);
            }
        };

        private String category;

        Category(String category) {
            this.category = category;
        }

        public String getCategory() {
            return this.category;
        }

        public abstract List<Product> getList();
    }

}
