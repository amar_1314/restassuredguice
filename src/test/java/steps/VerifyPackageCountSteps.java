package steps;

import com.google.inject.Inject;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import setup.RestSpec;

public class VerifyPackageCountSteps {

    private final RestSpec restSpec;

    @Inject
    public VerifyPackageCountSteps(RestSpec restSpec) {
        this.restSpec = restSpec;
    }

    @Given("^user on weborder page$")
    public void user_on_weborder_page() {
        // Write code here that turns the phrase above into concrete actions

    }

    @When("^user gets package count$")
    public void user_gets_package_count() {
//        driver.navigate().to("http://fpecomm.fpssidevqa.com");
//        WebDriverWait wait = new WebDriverWait(driver, 30);
//        wait.until(d ->
//                driver.getTitle().trim().equals("Get Secure"));
//        report.info("This is an extra info");

    }

    @Then("^user assert package count as (\\d+)$")
    public void user_assert_package_count_as(int count) {
        Assert.assertEquals(count, 3);
        System.out.println("In scenario 1 - " + restSpec.getWeborderRequestSpecification());

    }

}
