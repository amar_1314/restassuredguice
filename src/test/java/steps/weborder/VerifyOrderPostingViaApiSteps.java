package steps.weborder;

import api_request_resources.ContractType;
import api_request_resources.MonitoringPlan;
import api_request_resources.ShippingType;
import com.google.inject.Inject;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.Account;
import dto.Lead;
import dto.OrderModel;
import dto.Quote;
import pages.ecom.AccountPage;
import pages.ecom.LeadPage;
import pages.ecom.OrderPage;
import pages.ecom.QualificationPage;
import pages.ecom.QuotePage;
import reporting.Reporter;

import java.util.List;

public class VerifyOrderPostingViaApiSteps {

    private final LeadPage leadPage;
    private final QuotePage quotePage;
    private final QualificationPage qualificationPage;
    private final AccountPage accountPage;
    private final OrderPage orderPage;
    private final System.Logger logger;

    @Inject
    public VerifyOrderPostingViaApiSteps(LeadPage leadPage, QuotePage quotePage, QualificationPage qualificationPage, AccountPage accountPage, OrderPage orderPage, System.Logger logger) {
        this.leadPage = leadPage;
        this.quotePage = quotePage;
        this.qualificationPage = qualificationPage;
        this.accountPage = accountPage;
        this.orderPage = orderPage;
        this.logger = logger;
    }


    @And("^I post ecomm order via api$")
    public void iPostEcommOrderViaApi() {
        OrderModel orderModel = orderPage.postOrder();
        Reporter.addStepLog("Order has been processed successfully: " + orderModel.getOrderId());
        logger.log(System.Logger.Level.INFO, "Order has been processed: " + orderModel);
    }


    @Given("^I have lead info$")
    public void iHaveLeadInfo() {
        leadPage.prepareEcomLeadRequest();
    }

    @When("^I post ecomm lead via api$")
    public void iPostEcommLeadViaApi() {
        Lead lead = leadPage.postLead();
        Reporter.addStepLog("Lead created: " + lead.getLeadId());
        logger.log(System.Logger.Level.INFO, "Lead created: " + lead);
    }

    @And("^I post ecomm account via api$")
    public void iPostEcommAccountViaApi() {
        Account account = accountPage.postAccount();
        Reporter.addStepLog("Account created: " + account.getAccountId());
        logger.log(System.Logger.Level.INFO, "Account created: " + account);
    }

    @Then("^I verify ecomm account created via api$")
    public void iVerifyEcommAccountCreatedViaApi() {
        accountPage.VerifyAccountCreatedForInitialOrder();
    }

    @And("^I post ecomm quote via api$")
    public void iPostEcommQuoteViaApi() {
        Quote quote = quotePage.postQuote();
        Reporter.addStepLog("Quote created: " + quote.getQuoteId());
        logger.log(System.Logger.Level.INFO, "Quote created: " + quote);
    }

    @And("^I set contract term for ecomm quote$")
    public void iSetContractTermForEcommQuote(List<ContractType> contractTypeList) {
        quotePage.setContractType(contractTypeList.get(0));
    }

    @And("^I set plan for ecomm quote$")
    public void iSetPlanForEcommQuote(List<MonitoringPlan> planList) {
        quotePage.setMonitoringPlan(planList.get(0));
    }

    @And("^I set shipping for ecomm quote$")
    public void iSetShippingForEcommQuote(List<ShippingType> shippingTypeList) {
        quotePage.setShippingType(shippingTypeList.get(0));
    }

    @And("^I set products for ecomm quote$")
    public void iSetProductsForEcommQuote(List<String> productName) {
        quotePage.setProductSkuList(productName);
    }

}
