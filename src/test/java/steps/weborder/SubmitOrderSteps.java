package steps.weborder;

import com.github.javafaker.Faker;
import com.google.inject.Inject;
import common.ApplicationProperties;
import common.Assertions;
import common.SessionProvider;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.*;
import helper.Formatter;
import helper.Navigator;
import helper.Ui;
import models.Address;
import models.CustomerInfoModel;
import models.OrderProcessingTimes;
import models.ProductsModel;
import org.assertj.core.data.Offset;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import reporting.Reporter;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.fail;

public class SubmitOrderSteps {

    private final Ui ui;
    private final Navigator navigator;
    private final CustomerInfoModel customerInfoModel;
    private final CustomerInformationModel customerInformationModel;
    private final OrderProcessingTimes orderProcessingTimes;
    private final ApplicationProperties applicationProperties;
    private final SessionProvider sessionProvider;
    private final OrderModel orderModel;
    private final Assertions assertions;

    private By shopPackages = By.cssSelector("#maincontent [href='/shop-packages.html']");
    private By personalizeButton = By.cssSelector("button.personalize");
    private By buyNow = By.cssSelector("button.buy-now");
    private By reviewOrder = By.cssSelector("button.next-review-order");
    private By secureCheckout = By.cssSelector("button.primary.next-secure-checkout");
    private By loader = By.cssSelector(".loader [alt='Loading...']");

    private By firstName = By.cssSelector("[name='firstname']");
    private By lastName = By.cssSelector("[name='lastname']");
    private By email = By.cssSelector("[name='custom_attributes[email]']");
    private By primaryPhone = By.cssSelector("[name='telephone']");
    private By secondaryPhone = By.cssSelector("[name='custom_attributes[secondary_phone]']");
    private By userCode = By.cssSelector("[name='custom_attributes[user_code]']");
    private By passCode = By.cssSelector("[name='custom_attributes[verbal_security_password]']");

    private By address1 = By.cssSelector("[name='systemLocationAddress.street.0'] input");
    private By address2 = By.cssSelector("[name='systemLocationAddress.street.1'] input");
    private By city = By.cssSelector("[name='systemLocationAddress.city'] input");
    private By state = By.cssSelector("[name='systemLocationAddress.region_id'] select");
    private By zip = By.cssSelector("[name='systemLocationAddress.postcode'] input");

    private By addressVerificationModal = By.cssSelector("#address-selection-popup button");

    private By creditCheck = By.cssSelector("#shipping-method-buttons-container .continue");

    private By dateOfBirth = By.cssSelector(".credit_card_input_fields input");
    private By runCreditCheck = By.id("creditcheck_button");

    private By cardNumber = By.cssSelector("[name='payment[cc_number]']");
    private By expMonth = By.id("credit_card_month");
    private By expYr = By.id("credit_card_year");
    private By securityCode = By.cssSelector("[name='payment[cc_cid]']");
    private By nameOnCard = By.cssSelector("[name='payment[cc_name]']");
    private By monitoringAgreement = By.cssSelector("#payment_form .continue");

    private By checkbox = By.cssSelector(".screen-reader-text");
    private By btnContinueDesktop = By.id("action-bar-btn-continue");
    private By btnStartDesktop = By.id("navigate-btn");
    private By btnInitialsDesktop = By.cssSelector(".initials-tab-content");
    private By btnSignatureDesktop = By.cssSelector(".signature-tab-content");
    private By btnFinishDesktop = By.id("action-bar-btn-finish");
    private By adoptInitialsModalDesktop = By.id("adopt-dialog-content");
    private By btnAdoptInitialsDesktop = By.cssSelector("#adopt-dialog [data-ds='submit'][data-group-item='initials']");

    private By retailPrice = By.cssSelector(".addons-sidebar-summary [data-bind='html: cart().retail_price'] .price");
    private By discount = By.cssSelector("[data-bind='text: discount.formatted_total_discount']");
    private By shippingPrice = By.cssSelector("[data-bind='html: cart().shipping']");
    private By taxes = By.cssSelector("[data-bind='html: cart().taxes']");
    private By dueToday = By.cssSelector("[data-bind='html: cart().grand_total'] .price");

    private By placeOrder = By.cssSelector("#checkout-payment-method-load .checkout");
    private By shipping = By.cssSelector("tr.row");

    private By buildYourOwnButton = By.cssSelector(".primary[href='/build-your-own.html']");
    private By addonContainer = By.cssSelector(".addon-product-container");
    private By addonProductName = By.cssSelector(".addon-product-name");
    private By addonProductButton = By.cssSelector("button");
    private By addonQuantitySelect = By.cssSelector(".addon-product-qty-dropdown");
    private By monitoringPlanButton = By.cssSelector("button.next-review-order");

    private By emailWb = By.cssSelector("#sidebar-customer-info [ng-show='email'] span");
    private By ordersLinkSubNav = By.cssSelector("#sub-navigation-orders");
    private By initialOrderRow = By.cssSelector("tbody tr");
    private By subTotalWb = By.cssSelector("[ng-bind='$ctrl.totals.TotalWithTax | currency']");

    private By loginUserId = By.cssSelector("input[name='loginfmt']");
    private By nextButton = By.cssSelector("input[type='submit']");
    private By loginPassword = By.cssSelector("input[name='passwd']");
    private By signInButton = By.cssSelector("input[type='submit']");
    private By staySignedIn = By.cssSelector("input[type='submit']");

    private By releaseNotes = By.cssSelector(".btn[ng-click='vm.onClose()']");
    private By overlay = By.id("overlay-content");


    @Inject
    public SubmitOrderSteps(Ui ui, Navigator navigator, CustomerInfoModel customerInfoModel, CustomerInformationModel customerInformationModel, OrderProcessingTimes orderProcessingTimes, ApplicationProperties applicationProperties, SessionProvider sessionProvider, OrderModel orderModel, Assertions assertions) {
        this.ui = ui;
        this.navigator = navigator;
        this.customerInfoModel = customerInfoModel;
        this.customerInformationModel = customerInformationModel;
        this.orderProcessingTimes = orderProcessingTimes;
        this.applicationProperties = applicationProperties;
        this.sessionProvider = sessionProvider;
        this.orderModel = orderModel;
        this.assertions = assertions;
    }


    @Given("^I am on ecomm home page$")
    public void i_am_on_ecomm_home_page() {
        navigator.weborder().go();
    }

    @When("^I click on shop packages button$")
    public void i_click_on_shop_packages_button() {
        ui.findElement(shopPackages).scroll().click();
    }

    @When("^I click on buy now button on (.*) package$")
    public void i_click_on_buy_now_button_on_package(int pakg) {
        ui.getWait(90).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        ui.findElement(ui.getWait(30).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(buyNow)).get(pakg - 1))
                .scroll().click();
    }

    @When("^I click on review order button$")
    public void i_click_on_review_order_button() {
        ui.finishLoading();
        Ui.sleep(2000);
        ui.findElements(reviewOrder).stream().filter(WebElement::isDisplayed)
                .peek(ele -> ui.finishLoading())
                .findFirst()
                .ifPresentOrElse(ele -> {
                            ui.finishLoading();
                            ui.findElement(ele).jsClick();
                        },
                        () -> fail("Not able to find review order button"));
        ui.finishLoading();
        Assert.assertEquals("User not able to navigate out of review order page", 0, ui.getDriver().findElements(reviewOrder).size());
    }

    @When("^I click on secure checkout button$")
    public void i_click_on_secure_checkout_button() {
        ui.getWait(30).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        ui.findElement(secureCheckout).jsClick();
    }

    @When("^I enter customer information$")
    public void i_enter_customer_information() {
        ui.getWait(90).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        customerInformationModel.setFirstName("FPTest");
        customerInformationModel.setLastName("automatedTest");
        customerInformationModel.setEmail("test" + Instant.now().toEpochMilli() + "@fp.com");
        customerInformationModel.setPhone(Formatter.tenDigitNumber.format(new Faker().regexify("^[2-9][0-8][0-9]-?[2-9][0-9]{2}-?[0-9]{4}$")));
        customerInformationModel.setAltPhone(Formatter.tenDigitNumber.format(new Faker().regexify("^[2-9][0-8][0-9]-?[2-9][0-9]{2}-?[0-9]{4}$")));
        Address address = new Address();
        address.setAddress1("77 Green Acres Rd S");
        address.setAddress2("Apt 200");
        address.setCity("Yavapai County");
        address.setState("Arizona");
        address.setPostalCode("86303");
        customerInfoModel.setCustomerInformation(customerInformationModel);
        customerInfoModel.setShippingAddress(address);
        ui.findElement(firstName).scroll().sendKeys(customerInformationModel.getFirstName());
        ui.findElement(lastName).scroll().sendKeys(customerInformationModel.getLastName());
        ui.findElement(email).scroll().sendKeys(customerInformationModel.getEmail());
    }

    @When("^I enter emergency contact information$")
    public void i_enter_emergency_contact_information() {
        ui.getWait(90).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        ui.findElement(primaryPhone).scroll().sendKeys(customerInformationModel.getPhone());
        ui.getWait(90).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        ui.findElement(secondaryPhone).scroll().sendKeys(customerInformationModel.getAltPhone());
        ui.findElement(userCode).scroll().sendKeys("5555");
        ui.findElement(passCode).scroll().sendKeys("test");
    }

    @When("^I enter system location address$")
    public void i_enter_system_location_address() {
        ui.getWait(90).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        ui.findElement(address1).scroll().sendKeys(customerInfoModel.getShippingAddress().getAddress1());
        ui.findElement(address2).scroll().sendKeys(customerInfoModel.getShippingAddress().getAddress2());
        ui.findElement(city).scroll().sendKeys(customerInfoModel.getShippingAddress().getCity());
        Select selectState = new Select(ui.getWait(20).until(ExpectedConditions.visibilityOfElementLocated(state)));
        selectState.selectByVisibleText(customerInfoModel.getShippingAddress().getState());
        ui.findElement(zip).scroll().sendKeys(customerInfoModel.getShippingAddress().getPostalCode());
        ui.getWait(60).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        Ui.sleep(2000);
        ui.getWait(90).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        ui.getWait(60).until(ExpectedConditions
                .and(
                        ExpectedConditions.visibilityOfElementLocated(addressVerificationModal),
                        ExpectedConditions.elementToBeClickable(addressVerificationModal)
                )
        );
        ui.findElement(addressVerificationModal).jsClick();
    }

    @When("^I select (.*) shipping method$")
    public void i_select_shipping_method(int shippingMethod) {
        ui.getWait(90).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        ui.getDriver().findElements(shipping).get(shippingMethod).click();
    }

    @When("^I click on credit check button$")
    public void i_click_on_credit_check_button() {
        ui.getWait(90).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        ui.getWait(20).until(ExpectedConditions.elementToBeClickable(creditCheck)).click();
    }

    @When("^I enter date of birth$")
    public void i_enter_date_of_birth() {
        ui.getWait(90).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        ui.findElement(dateOfBirth).scroll().sendKeys("11");
        Ui.sleep();
        ui.findElement(dateOfBirth).scroll().sendKeys("11");
        Ui.sleep();
        ui.findElement(dateOfBirth).scroll().sendKeys("1990");
    }

    @When("^I run credit check$")
    public void i_run_credit_check() {
        ui.getWait(90).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        ui.findElement(runCreditCheck).jsClick();
    }

    @When("^I fill in credit card information$")
    public void i_fill_in_credit_card_information() {
        ui.getWait(90).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        ui.findElement(cardNumber).scroll().sendKeys("4111111111111111");
        Select expMonthSelect = new Select(ui.findElement(expMonth).scroll());
        expMonthSelect.selectByVisibleText("January");
        Select expYrSelect = new Select(ui.findElement(expYr).scroll());
        expYrSelect.selectByVisibleText("2020");
        ui.findElement(securityCode).scroll().sendKeys("555");
        ui.findElement(nameOnCard).scroll().sendKeys("test test");
        ui.findElement(monitoringAgreement).scroll().click();
    }

    @When("^I sign contract$")
    public void i_sign_contract() {
        ui.getWait(120).until(ExpectedConditions.invisibilityOfElementLocated(loader));
        Ui.sleep(2000);
        ui.getDriver().switchTo().frame(ui.getDriver().findElement(By.id("monitor-agreement")));
        Ui.sleep(3000);
        Reporter.addScreenCapture(ui.getScreenshot(), "Signing contract");
        ui.findElement(checkbox).jsClick();
        ui.getWait(30).until(ExpectedConditions.elementToBeClickable(btnContinueDesktop)).click();
        ui.getWait(30).until(ExpectedConditions.elementToBeClickable(btnStartDesktop)).click();
        Ui.sleep();
        List<WebElement> initials = ui.getWait(30).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(btnInitialsDesktop));
        initials.get(0).click();
        Ui.sleep();
        ui.getWait(60).until(ExpectedConditions.visibilityOfElementLocated(adoptInitialsModalDesktop));
        ui.findElement(btnAdoptInitialsDesktop).jsClick();
        Ui.sleep(2000);
        initials.stream()
                .skip(1)
                .peek(ele -> ui.getWait(20).until(ExpectedConditions.elementToBeClickable(ele)))
                .forEach(ele -> ui.findElement(ele).jsClick());
        ui.findElement(btnSignatureDesktop).jsClick();
        ui.findElement(btnFinishDesktop).jsClick();
        ui.getDriver().switchTo().defaultContent();
        Ui.sleep();
    }

    @Then("^I place order$")
    public void i_place_order() {
        ui.getWait(90).until(ExpectedConditions.and(
                ExpectedConditions.invisibilityOfElementLocated(loader),
                ExpectedConditions.elementToBeClickable(placeOrder)
        ));
        Reporter.addScreenCapture(ui.getScreenshot(), "Placing order");
        ui.findElement(placeOrder).jsClick();
        orderProcessingTimes.setSubmissionTime(LocalDateTime.now());
        Reporter.addStepLog("Submission time: " + orderProcessingTimes.getSubmissionTime());
    }

    @Then("^I verify if order has been submitted$")
    public void i_verify_if_order_has_been_submitted() {
        ui.getWait(120).until(ExpectedConditions.titleContains("Success Page"));
        Reporter.addScreenCapture(ui.getScreenshot(), "Order placed");
        orderProcessingTimes.setProcessedTime(LocalDateTime.now());
        Reporter.addStepLog("Processed time: " + orderProcessingTimes.getProcessedTime());
        System.out.println("Time: " + Duration.between(orderProcessingTimes.getSubmissionTime(), orderProcessingTimes.getProcessedTime()).toSeconds());
        Reporter.addStepLog("Time took for order to process: " + Duration.between(orderProcessingTimes.getSubmissionTime(), orderProcessingTimes.getProcessedTime()).toSeconds());
    }

    @Then("^I verify account is accessible in workbench$")
    public void i_verify_account_is_accessible_in_workbench() {

        Reporter.addStepLog("Querying for leadId with email: " + customerInformationModel.getEmail() + " in " + applicationProperties.environment + " environment");

        LeadRawInfo leadRawInfo = sessionProvider
                .selectFrom(LeadRawInfo.class).where(LeadRawInfo_.EMAIL)
                .isEqualTo(customerInformationModel.getEmail()).buildQuery().get(LeadRawInfo.class);

        Lead lead = sessionProvider.selectFrom(Lead.class).where(Lead_.RAW_LEAD_ID).isEqualTo(leadRawInfo.getRawLeadId())
                .where(Lead_.PARENT_LEAD_ID).isNull().buildQuery().get(Lead.class);

        Account account = sessionProvider.selectFrom(Account.class).where(Account_.LEAD_ID).isEqualTo(lead.getLeadId()).buildQuery()
                .get(Account.class);

        OrderModel order = sessionProvider.selectFrom(OrderModel.class).where(OrderModel_.ACCOUNT_ID).isEqualTo(account.getAccountId())
                .where(OrderModel_.QUOTE_ID).isNotNull().buildQuery().get(OrderModel.class);
        orderModel.setOrderId(order.getOrderId());
        navigator.wb().go("accounts/" + account.getAccountId() + "/summary");
        login();
        ui.getWait(60).until(ExpectedConditions.and(
                ExpectedConditions.invisibilityOfElementLocated(overlay),
                ExpectedConditions.visibilityOfElementLocated(releaseNotes)
        ));
        ui.getDriver().findElements(releaseNotes).forEach(WebElement::click);
        ui.getWait(60).until(ExpectedConditions.invisibilityOfElementLocated(overlay));
        Reporter.addScreenCapture(ui.getScreenshot(), "Navigated to workbench for account " + account.getAccountId());
        Assert.assertEquals(customerInformationModel.getEmail(), ui.getWait(120).until(ExpectedConditions.visibilityOfElementLocated(emailWb)).getText());
        ui.findElement(ordersLinkSubNav).jsClick();
        ui.findElement(initialOrderRow).jsClick();
        ui.findElement(subTotalWb).scroll();
        Reporter.addScreenCapture(ui.getScreenshot(), "Navigated to orders page");
    }

    @When("^I click on build your own button$")
    public void i_click_on_build_your_own_button() {
        ui.findElement(buildYourOwnButton).jsClick();
    }

    @When("^I add products$")
    public void i_add_products(List<ProductsModel> productList) {
        Map<String, Integer> products = productList.stream().collect(Collectors.toMap(ProductsModel::getProduct, ProductsModel::getQuantity));
        ui.getWait(30).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(addonContainer))
                .stream().filter(element -> products.keySet().contains(element.findElement(addonProductName).getText().trim()))
                .forEach(element -> {
                    Select quantitySelect = new Select(ui.findElement(element.findElement(addonQuantitySelect)).scroll());
                    Ui.sleep(2000);
                    ui.finishLoading();
                    quantitySelect.selectByIndex(products.get(ui.findElement(element.findElement(addonProductName)).scroll().getText().trim()) - 1);
                    ui.findElement(element.findElement(addonProductButton)).jsClick();
                });
    }

    @When("^I click on monitoring plan button$")
    public void i_click_on_monitoring_plan_button() {

        ui.findElements(monitoringPlanButton).stream().filter(WebElement::isDisplayed).findFirst().orElseThrow().click();
    }

    @And("^I click on personalize solution button for package (.*)$")
    public void iClickOnPersonalizeSolutionButtonForPackage(int pkgNumber) {
        ui.findElements(personalizeButton).get(pkgNumber - 1).click();
    }

    @And("^I grab order details on review page$")
    public void iGrabOrderDetailsOnReviewPage() {
        ui.finishLoading();
        orderModel.setSubtotalPrice(Formatter.plainPriceFormat.format(ui.findElement(retailPrice).scroll().getText()));
        orderModel.setDiscountContractPrice(getContractDiscount());
        orderModel.setDiscountCouponPrice(getAdditionalDiscount());
        orderModel.setShippingPrice(Formatter.plainPriceFormat.format(ui.findElement(shippingPrice).scroll().getText()));
        orderModel.setTaxTotal(Formatter.plainPriceFormat.format(ui.findElement(taxes).scroll().getText()));
        orderModel.setTotalWithTax(Formatter.plainPriceFormat.format(ui.findElement(dueToday).scroll().getText()));
    }

    @And("^I verify order details on workbench orders page$")
    public void iVerifyOrderDetailsOnWorkbenchOrdersPage() {
        OrderModel order = sessionProvider.selectFrom(OrderModel.class)
                .where(OrderModel_.ORDER_ID)
                .isEqualTo(orderModel.getOrderId())
                .buildQuery()
                .get(OrderModel.class);
        assertions.assertThat(order.getSubtotalPrice())
                .as("Retail price")
                .isEqualTo(orderModel.getSubtotalPrice());
        assertions.assertThat(Formatter.usCurrency.format(order.getDiscountCouponPrice()))
                .as("Additional discount")
                .isEqualTo(Formatter.usCurrency.format(orderModel.getDiscountCouponPrice()));
        assertions.assertThat(order.getDiscountContractPrice())
                .as("Contract discount")
                .isEqualTo(orderModel.getDiscountContractPrice());
        assertions.assertThat(order.getShippingPrice())
                .as("Shipping price")
                .isEqualTo(orderModel.getShippingPrice());
        double taxCorrection = 0.50;
        assertions.assertThat(order.getTaxTotal())
                .as("Total tax")
                .isCloseTo(orderModel.getTaxTotal(), Offset.offset(taxCorrection));
        assertions.assertThat(order.getTotalWithTax())
                .as("Total with tax")
                .isCloseTo(orderModel.getTotalWithTax(), Offset.offset(taxCorrection));
        assertions.assertAll();

    }

    @And("^I select (.*) plan$")
    public void iSelectPlan(PlanTypeEnum plan) {
        String planTypeElementString = "[data-product-sku='planTypeEnum'] button.primary";
        By planTypeElement = By.cssSelector(planTypeElementString.replaceAll("planTypeEnum", plan.toString().toLowerCase()));
        if (ui.findElements(planTypeElement).size() > 0) {
            ui.findElement(planTypeElement).jsClick();
        }
    }

    private double getContractDiscount() {
        String contractDiscountString = ui.findElements(discount, true).get(0).getText();
        return Formatter.plainPriceFormat.format(contractDiscountString);
    }

    private double getAdditionalDiscount() {
        if (ui.findElements(discount, true).size() <= 1) {
            return 0.0;
        }
        return ui.findElements(discount, true).stream()
                .skip(1)
                .mapToDouble(additionalDiscount -> Formatter.plainPriceFormat.format(additionalDiscount.getText()))
                .sum();
    }

    private void login() {
        ui.getWait(30).until(ExpectedConditions.elementToBeClickable(loginUserId)).sendKeys(applicationProperties.wb.defaultUser.username);
        ui.findElement(nextButton).jsClick();
        ui.getWait(30).until(ExpectedConditions.elementToBeClickable(loginPassword)).sendKeys(applicationProperties.wb.defaultUser.password);
        ui.findElement(signInButton).jsClick();
        ui.findElement(staySignedIn).jsClick();
        Ui.sleep(2000);
    }

}