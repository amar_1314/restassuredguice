package steps.weborder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.filter.log.RequestLoggingFilter;
import com.jayway.restassured.http.ContentType;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import setup.RestSpec;

import javax.json.Json;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.jayway.restassured.RestAssured.given;

@ScenarioScoped
public class VerifyCachingTimeResponseSteps {

    private final ObjectMapper mapper;
    private final RestSpec restSpec;

    private List<Url> urls;
    private double averageResponseTime = 0;
    private String url;

    @Inject
    public VerifyCachingTimeResponseSteps(ObjectMapper mapper, RestSpec restSpec) {
        this.mapper = mapper;
        this.restSpec = restSpec;
    }

    @Given("^I have all api calls to make that can be cached$")
    public void i_have_all_api_calls_to_make_that_can_be_cached() {
        urls = getUrls();
    }

    @When("^I make api calls for \"([^\"]*)\" times before cache$")
    public void i_make_api_calls_for_times_before_cache(int times) {
        averageResponseTime = IntStream.rangeClosed(1, times).mapToDouble(i ->
                getAverageTime()
        ).average().orElse(Double.NaN);
    }

    @Then("^I capture response time$")
    public void i_capture_response_time() {
        System.out.println("Average time in milliseconds: " + averageResponseTime);
//        report.info("Average time in milliseconds: " + averageResponseTime);
    }

    @When("^I make api calls from different sources for \"([^\"]*)\" times before cache$")
    public void i_make_api_calls_from_different_sources_for_times_before_cache(int times) {
        averageResponseTime = IntStream.rangeClosed(1, times)
                .mapToDouble(i ->
                        getAverageTimeFromDifferentSources()).average().orElse(Double.NaN);

    }

    @When("^I make api calls from all different sources for \"([^\"]*)\" times before cache$")
    public void i_make_api_calls_from_all_different_sources_for_times_before_cache(int times) {
        averageResponseTime = IntStream.rangeClosed(1, times)
                .mapToDouble(i ->
                        getAverageTimeFromAllDifferentSources()).average().orElse(Double.NaN);

    }

    @Given("^I have tax transaction call url$")
    public void i_have_tax_transaction_call_url() {
        Url url = new Url();
        url.setUrl("https://qa-apigateway.fpssi.com/tax/v1/transactions");
        urls = List.of(new Url[]{url});
    }

    @When("^I make api call for tax transaction for \"([^\"]*)\" times$")
    public void i_make_api_call_for_tax_transaction_for_times(int times) {
        IntStream.rangeClosed(1, 1)
                .forEach(i->System.out.println("Call "+i+": "+getAverageTimeForTaxTransactionCall()));


//        averageResponseTime = IntStream.rangeClosed(1, times)
//                .mapToDouble(i ->
//                        getAverageTimeForTaxTransactionCall()).average().orElse(Double.NaN);
    }

    @Given("^I have \"([^\"]*)\" api url$")
    public void i_have_api_url(String url) {
        this.url = url;
    }

    @When("^I make api call$")
    public void i_make_api_call() {
        IntStream.rangeClosed(1, 10)
                .forEach(i -> System.out.println("Call " + i + ": " + getSingleCallTime()));
    }

    private double getSingleCallTime(){
        return given()
                .spec(new RequestSpecBuilder()
                        .addHeader("Authorization", "bearer 7884967bf339feab7718821ad6b973dc")
                        .setContentType(ContentType.JSON).build())
                .when()
                .get(this.url)
                .then()
                .statusCode(200)
                .extract()
                .timeIn(TimeUnit.MILLISECONDS);
    }

    private double getAverageTimeForTaxTransactionCall() {
        return urls.stream().parallel()
                .mapToLong(url ->
                        given()
                                .spec(new RequestSpecBuilder()
                                        .addHeader("Authorization", "bearer dc21981534bd48e2e79a97e55ea6b6fc")
                                        .setContentType(ContentType.JSON)
                                        .setBody(getJsonForTaxTransaction()).build())
                                .when()
                                .post(url.getUrl())
                                .then()
                                .statusCode(200)
                                .extract()
                                .timeIn(TimeUnit.MILLISECONDS)
                ).average().orElse(Double.NaN);
    }

    private String getJsonForTaxTransaction(){

        TaxTransactionBody body = new TaxTransactionBody(
                159368,
                8,
                "Ground",
                new EcommAddress("1595 Spring Hill rd", "ste 110", "Vienna", "VA", "US", "22182", "Test account", 3, 0, String.valueOf(Instant.now())),
                new EcommAddress("1595 Spring Hill rd", "ste 110", "Vienna", "VA", "US", "22182", "Test account", 3, 0, String.valueOf(Instant.now())),
                0,
                8,
                "",
                "",
                "SalesInvoice",
                true,
                new TaxableItems[]{
                        new TaxableItems(40.99, "RE122", 0, false)
                }
        );
        String jsonString = null;
        try{
         jsonString = mapper.writeValueAsString(body);
        }catch (Exception e){
            //
        }


        return jsonString;
//                                        .addFilter(new RequestLoggingFilter())
    }

    private double getAverageTimeFromAllDifferentSources() {
        return urls.stream().parallel()
                .mapToLong(url ->
                        given().spec(new RequestSpecBuilder()
                                .addHeader("Authorization", "bearer " + getToken())
                                .addFilter(new RequestLoggingFilter())
                                .build())
                                .when()
                                .get(url.getUrl())
                                .then()
                                .statusCode(200)
                                .extract()
                                .timeIn(TimeUnit.MILLISECONDS)
                ).average().orElseThrow();
    }

    private String getToken() {
        return given()
                .contentType(ContentType.JSON)
                .body(Json.createObjectBuilder()
                        .add("Username", "test7970@fp.com")
                        .add("Password", "test1234")
                        .add("RememberMe", false)
                        .build().toString())
                .when()
                .post("https://api-qa-tokens.azurewebsites.net/token/generate")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .as(Token.class)
                .getToken();
    }

    private double getAverageTimeFromDifferentSources() {
        return urls.stream().parallel()
                .mapToLong(url ->
                        given().spec(new RequestSpecBuilder()
                                .addHeader("Authorization", "bearer " + url.getToken())
                                .addFilter(new RequestLoggingFilter())
                                .build())
                                .when()
                                .get(url.getUrl())
                                .then()
                                .statusCode(200)
                                .extract()
                                .timeIn(TimeUnit.MILLISECONDS)
                ).average().orElseThrow();
    }

    private List<Url> getUrls() {
        try (Stream<String> UrlFileLines = Files.lines(Paths.get("src/test/dto/ecom/urlsWithToken.json"))) {
            String urlString = UrlFileLines.collect(Collectors.joining("\n"));
            return List.of(mapper.readValue(urlString, Url[].class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private double getAverageTime() {
        return urls.stream().parallel().mapToLong(url ->
                given().spec(restSpec.getCpRequestSpecification())
                        .when()
                        .get(url.getUrl())
                        .then()
                        .statusCode(200)
                        .extract()
                        .response()
                        .getTimeIn(TimeUnit.MILLISECONDS)
        ).average().orElseThrow();
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Token {
        private String token;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Url {
        private String url;
        private String token;
    }

    @Data
    @AllArgsConstructor
    @ToString
    private static class TaxTransactionBody {
        private int customerCode;
        private double shippingAmount;
        private String shippingMethod;
        private EcommAddress customerAddress;
        private EcommAddress companyAddress;
        private double discount;
        private double shippingDiscount;
        private String taxExemptionCode;
        private String taxTransactionCode;
        private String documentType;
        private boolean commit;
        private TaxableItems[] taxableLineItems;
    }

    @Data
    @AllArgsConstructor
    private static class EcommAddress {
        private String line1;
        private String line2;
        private String city;
        private String state;
        private String country;
        private String postalCode;
        private String attention;
        private int addressType;
        private int addressID;
        private String dateCreated;
    }

    @Data
    @AllArgsConstructor
    private static class TaxableItems {
        private double amount;
        private String itemCode;
        private int itemType;
        private boolean taxIncluded;
    }
}
