package steps.weborder;

import com.google.inject.Inject;
import common.Assertions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import dto.ContractType;
import dto.Qualification;
import helper.HelperFunctions;
import org.junit.Assert;
import pages.ecom.QualificationPage;
import reporting.Reporter;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class VerifyCreditOverrideFlagFunctionalitySteps {

    private final QualificationPage qualificationPage;
    private final Assertions assertions;
    private final System.Logger logger;


    @Inject
    public VerifyCreditOverrideFlagFunctionalitySteps(QualificationPage qualificationPage, Assertions assertions, System.Logger logger) {
        this.qualificationPage = qualificationPage;
        this.assertions = assertions;
        this.logger = logger;
    }

    @And("^I verify if user is qualified for all contract types via api$")
    public void iVerifyIfUserIsQualifiedForAllContractTypesViaApi() {
        List<ContractType> contractTypeListDb = qualificationPage.getContractTypesDb(true)
                .stream()
                .sorted(Comparator.comparingInt(ContractType::getContractTypeId))
                .collect(Collectors.toList());
        List<ContractType> contractTypeListApi = List.of(qualificationPage.getContractTypesApi())
                .stream()
                .sorted(Comparator.comparingInt(ContractType::getContractTypeId))
                .collect(Collectors.toList());
        Reporter.addStepLog("Verifying contract types: " + HelperFunctions.prettyFormat(contractTypeListApi));

        Assert.assertEquals("Contract types from DB is not matching with api response", contractTypeListDb.size(), contractTypeListApi.size());


        for (int i = 0; i < contractTypeListDb.size(); i++) {
            assertions.assertThat(contractTypeListApi.get(i).getContractTypeId())
                    .as("Contract type id")
                    .isEqualTo(contractTypeListDb.get(i).getContractTypeId());
            assertions.assertThat(contractTypeListApi.get(i).getTerm())
                    .as("Contract type term")
                    .isEqualTo(contractTypeListDb.get(i).getTerm());
        }
        assertions.assertAll();
    }

    @And("^I verify if user is qualified only for one year contract$")
    public void iVerifyIfUserIsQualifiedOnlyForOneYearContract() {

        List<ContractType> contractTypeListDb = qualificationPage.getContractTypesDb(api_request_resources.ContractType.OneYear);
        Assert.assertEquals("Got more than one record for one year contract from DB", 1, contractTypeListDb.size());
        ContractType contractTypeDb = contractTypeListDb.get(0);
        List<ContractType> contractTypeListApi = List.of(qualificationPage.getContractTypesApi());
        Reporter.addStepLog("Verifying contract types: " + HelperFunctions.prettyFormat(contractTypeListApi));
        Assert.assertEquals("Got more than one record for one year contract from Api", 1, contractTypeListApi.size());
        ContractType contractTypeApi = contractTypeListApi.get(0);
        Assert.assertEquals("Contract type id is not matching", contractTypeDb.getContractTypeId(), contractTypeApi.getContractTypeId());
    }

    @Then("^I post ecomm qualification via api with isCreditOverride flag \"([^\"]*)\" and first name to get valid credit score to \"([^\"]*)\"$")
    public void iPostEcommQualificationViaApiWithIsCreditOverrideFlagAndFirstNameToGetValidCreditScoreTo(boolean isCreditOverride, boolean firstnameForValidCreditScore) {
        qualificationPage.prepareEcomQualificationRequest(isCreditOverride, firstnameForValidCreditScore);
        Reporter.addStepLog("Posting qualification with isCreditOverrideFlag: " + isCreditOverride + " and first name to get valid credit score: " + firstnameForValidCreditScore);
        Qualification qualification = qualificationPage.postQualification();
        Reporter.addStepLog("Qualification posted: " + qualification.getQualificationId());
        logger.log(System.Logger.Level.INFO, "Qualification posted: " + qualification);
    }

    @And("^I verify if user is qualified for all contract types but month to month via api$")
    public void iVerifyIfUserIsQualifiedForAllContractTypesButMonthToMonthViaApi() {
        List<ContractType> contractTypeListDb = qualificationPage.getNonMonthToMonthContractTypesDb();
        List<ContractType> contractTypeListApi = List.of(qualificationPage.getContractTypesApi());
        Reporter.addStepLog("Verifying contract types: " + HelperFunctions.prettyFormat(contractTypeListApi));
        Assert.assertEquals("Contract types from DB is not matching with api response", contractTypeListDb.size(), contractTypeListApi.size());


        for (int i = 0; i < contractTypeListDb.size(); i++) {
            assertions.assertThat(contractTypeListApi.get(i).getContractTypeId())
                    .as("Contract type id")
                    .isEqualTo(contractTypeListDb.get(i).getContractTypeId());
            assertions.assertThat(contractTypeListApi.get(i).getTerm())
                    .as("Contract type term")
                    .isEqualTo(contractTypeListDb.get(i).getTerm());
        }
        assertions.assertAll();


    }
}
