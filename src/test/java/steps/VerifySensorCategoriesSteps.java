package steps;

import com.google.inject.Inject;
import common.Assertions;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helper.Ui;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.CpShopPage;

public class VerifySensorCategoriesSteps {

    private final CpShopPage cpShopPage;
    private final Assertions assertions;
    private final Ui ui;
    private final By leftNav = By.cssSelector("#app-nav");
    private final By myAccountTab = By.linkText("My Account");
    private final By shopTab = By.linkText("Shop");
    private final By camerasTab = By.linkText("Cameras");

    @Inject
    public VerifySensorCategoriesSteps(CpShopPage cpShopPage, Assertions assertions, Ui ui) {
        this.cpShopPage = cpShopPage;
        this.assertions = assertions;
        this.ui = ui;
    }

    @Given("^I query for sensor category list$")
    public void i_query_for_sensor_category_list() {
        cpShopPage.queryForSensorCategoryList();
    }

    @Then("^I verify sensor category list$")
    public void i_verify_sensor_category_list() {

        assertions.assertThat(cpShopPage.getActualSensorCategories()).as("Category List").isEqualTo(cpShopPage.getSensorCategories());
        assertions.assertAll();
    }

    @Given("^I query for product list$")
    public void i_query_for_product_list() {
        cpShopPage.queryForProductList();
    }

    @Then("^I verify product count$")
    public void i_verify_product_count() {
        assertions.assertThat(cpShopPage.getSensorCount()).as("Sensor count in product list").isEqualTo(9);
        assertions.assertThat(cpShopPage.getCameraCount()).as("Camera count in product list").isEqualTo(3);
        assertions.assertThat(cpShopPage.getHomeAutomationCount()).as("Home automation count in product list").isEqualTo(5);
        assertions.assertAll();
    }

    @When("^I click on MyAccount tab$")
    public void i_click_on_MyAccount_tab() {
        ui.getWait(20).until(ExpectedConditions.elementToBeClickable(leftNav).andThen((ele) -> ele.findElement(myAccountTab))).click();
    }

    @When("^I click on Shop tab$")
    public void i_click_on_Shop_tab() {
        ui.getWait(20).until(ExpectedConditions.elementToBeClickable(leftNav).andThen(ele -> ele.findElement(shopTab))).click();
    }

    @When("^I click on Cameras tab$")
    public void i_click_on_Cameras_tab() {
        ui.getWait(45).until(ExpectedConditions.elementToBeClickable(leftNav).andThen(ele-> ele.findElement(camerasTab))).click();
    }

    @Then("^I verify cameras displayed$")
    public void i_verify_cameras_displayed() {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I verify cameras cost$")
    public void i_verify_cameras_cost() {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
