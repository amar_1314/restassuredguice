package steps.customerportal;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class MovingFormPageFunctionalitySteps {

    @When("^I navigate to move portal page$")
    public void i_navigate_to_move_portal_page() {


    }

    @When("^I do not have a pending move$")
    public void i_do_not_have_a_pending_move() {

    }

    @Then("^I click on start move button$")
    public void i_click_on_start_move_button() {

    }

    @Then("^I see premise address is pre-filled in the mover's kit address section$")
    public void i_see_premise_address_is_pre_filled_in_the_mover_s_kit_address_section() {

    }
}
