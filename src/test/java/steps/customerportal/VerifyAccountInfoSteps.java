package steps.customerportal;

import com.google.inject.Inject;
import common.ApplicationProperties;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helper.Navigator;
import helper.Ui;
import models.AccountInfo;
import models.CustomerInfoModel;
import dto.CustomerInformationModel;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.AccountSettingsPage;
import pages.LoginPage;
import setup.RestSpec;

import java.util.List;

import static com.jayway.restassured.RestAssured.given;

public class VerifyAccountInfoSteps {

    private final ApplicationProperties properties;
    private final Ui ui;
    private final LoginPage loginPage;
    private final AccountSettingsPage accountSettingsPage;
    private final RestSpec restSpec;
    private final Navigator navigator;

    private final AccountInfo accountInfo;

    private By logoutButton = By.cssSelector("[ng-click='$ctrl.logout()']");
    private By loginButton = By.cssSelector("[type='submit']");
    private By username = By.cssSelector("#form-login [name='login-username']");
    private By password = By.cssSelector("#form-login [name='login-password']");

    @Inject
    public VerifyAccountInfoSteps(ApplicationProperties properties, Ui ui, LoginPage loginPage, AccountSettingsPage accountSettingsPage, RestSpec restSpec, Navigator navigator, AccountInfo accountInfo) {
        this.ui = ui;
        this.properties = properties;
        this.loginPage = loginPage;
        this.accountSettingsPage = accountSettingsPage;
        this.restSpec = restSpec;
        this.navigator = navigator;
        this.accountInfo = accountInfo;
    }

    @Given("^I am on customerportal app login page$")
    public void i_am_on_customerportal_app_login_page() {
        navigator.cp().go("/login");
        Assert.assertTrue(ui.getWait(30).until(ExpectedConditions.visibilityOfElementLocated(loginButton)).isEnabled());
    }

    @When("^I type in default user username$")
    public void i_type_in_default_user_username() {
        ui.getWait(30)
                .until(ExpectedConditions.visibilityOfElementLocated(username))
                .sendKeys(properties.cp.defaultUser.username);
    }

    @When("^I type in default user password$")
    public void i_type_in_default_user_password() {
        ui.getWait(30)
                .until(ExpectedConditions.visibilityOfElementLocated(password))
                .sendKeys(properties.cp.defaultUser.password);
    }

    @When("^I click on submit login$")
    public void i_click_on_submit_login() {
        ui.getWait(30).until(ExpectedConditions.elementToBeClickable(loginButton)).click();

    }

    @Then("^I logged into customerportal app successfully$")
    public void i_logged_into_customerportal_app_successfully() {
        Assert.assertTrue(ui.getWait(30).until(ExpectedConditions.visibilityOfElementLocated(logoutButton)).isEnabled());
    }

    @Given("^I log into customerportal app as default user$")
    public void i_log_into_customerportal_app_as_default_user() {
        loginPage.loginAsDefaultUser();
    }

    @Then("^I query for account info$")
    public void i_query_for_account_info() {
        accountInfo.customerInformation = given()
                .spec(restSpec.getCpRequestSpecification())
                .when()
                .get("api/Account/CustomerInformation")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .as(CustomerInformationModel.class);
        accountInfo.customerInfo = given()
                .spec(restSpec.getCpRequestSpecification())
                .when()
                .get("api/Account/CustomerInfo")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .as(CustomerInfoModel.class);
    }

    @Then("^I verify account info$")
    public void i_verify_account_info() {
        accountSettingsPage.verityAccountInfo();
    }

    @Given("^I edit account info with$")
    public void i_edit_account_info_with(List<CustomerInformationModel> customerInformationList) {
        accountSettingsPage.editAccountInfoWith(customerInformationList.get(0));
        accountSettingsPage.saveAccountInfo();
    }
}
