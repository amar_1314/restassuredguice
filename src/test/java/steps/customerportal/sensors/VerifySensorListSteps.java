package steps.customerportal.sensors;

import com.google.inject.Inject;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import models.DeviceList;
import pages.customerportal.sensors.SensorListPage;

public class VerifySensorListSteps {

    private final SensorListPage sensorListPage;
    private final System.Logger logger;
    private final DeviceList deviceList;

    @Inject
    public VerifySensorListSteps(SensorListPage sensorListPage, System.Logger logger, DeviceList deviceList) {
        this.sensorListPage = sensorListPage;
        this.logger = logger;
        this.deviceList = deviceList;
    }


    @Given("^I query for sensor list from adc$")
    public void iQueryForSensorListFromAdc() {
        deviceList.setAdcDeviceList(sensorListPage.getSensorListFromAdc());
        logger.log(System.Logger.Level.INFO, "Setting adc device list: " + deviceList.getAdcDeviceList());
    }

    @When("^I query for sensor list from customer portal$")
    public void iQueryForSensorListFromCustomerPortal() {
        deviceList.setCpDeviceList(sensorListPage.getSensorListFromCp());
        logger.log(System.Logger.Level.INFO, "Setting cp device list: " + deviceList.getCpDeviceList());
    }

    @Then("^I compare sensor list from adc and customer portal$")
    public void iCompareSensorListFromAdcAndCustomerPortal() {
        deviceList.setAdcDeviceList(sensorListPage.updateAdcDeviceListWithGroupNames(deviceList.getAdcDeviceList()));
        logger.log(System.Logger.Level.INFO, "Updating adc device list with group name: " + deviceList.getAdcDeviceList());
        sensorListPage.compareDeviceListFromAdcAndCp(deviceList);
    }

    @Then("^I verify device categories on sensors page$")
    public void iVerifyDeviceCategoriesOnSensorsPage() {
        deviceList.setAdcDeviceList(sensorListPage.updateAdcDeviceListWithGroupNames(deviceList.getAdcDeviceList()));
        SensorListPage.DeviceList adcDeviceList = deviceList.getAdcDeviceList();
        SensorListPage.DeviceList cpDeviceList = deviceList.getCpDeviceList();
        sensorListPage.verifyDeviceCategories(adcDeviceList, cpDeviceList);
    }

    @Then("^I verify device names on sensors page$")
    public void iVerifyDeviceNamesOnSensorsPage() {
        deviceList.setAdcDeviceList(sensorListPage.updateAdcDeviceListWithGroupNames(deviceList.getAdcDeviceList()));
        SensorListPage.DeviceList adcDeviceList = deviceList.getAdcDeviceList();
        SensorListPage.DeviceList cpDeviceList = deviceList.getCpDeviceList();
        sensorListPage.verifyDeviceNames(adcDeviceList, cpDeviceList);
    }
}
