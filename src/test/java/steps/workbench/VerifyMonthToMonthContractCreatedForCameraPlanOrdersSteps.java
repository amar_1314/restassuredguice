package steps.workbench;

import com.google.inject.Inject;
import common.Assertions;
import common.SessionProvider;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import dto.ContractType;
import dto.Quote;
import dto.Product;
import helper.TestDataHandler;
import pages.workbench.InitialOrderPage;
import pages.workbench.QuotePage;

import java.util.ArrayList;
import java.util.List;

public class VerifyMonthToMonthContractCreatedForCameraPlanOrdersSteps {

    private static List<ContractType> contractTypeList = new ArrayList<>();
    private final SessionProvider sessionProvider;
    private final Assertions assertions;
    private final QuotePage quotePage;
    private final InitialOrderPage initialOrderPage;


    @Inject
    public VerifyMonthToMonthContractCreatedForCameraPlanOrdersSteps(SessionProvider sessionProvider, Assertions assertions, QuotePage quotePage, InitialOrderPage initialOrderPage) {
        this.sessionProvider = sessionProvider;
        this.assertions = assertions;
        this.quotePage = quotePage;
        this.initialOrderPage = initialOrderPage;
    }

    @Given("^I query contractType table$")
    public void iQueryContractTypeTable() {
        contractTypeList = sessionProvider.selectFrom(ContractType.class).buildQuery().getList(ContractType.class);
    }

    @Then("^I verify Month-to-Month contract type is returned$")
    public void iVerifyMonthToMonthContractTypeIsReturned() {
        List<ContractType> expectedContractList = TestDataHandler.contractTypes.getList(ContractType.class);
        ContractType expectedMonthToMonthContractType = expectedContractList.stream().filter(contractType -> contractType.getContractTypeId() == 4)
                .findFirst().orElseThrow();
        ContractType actualMonthToMonthContractType = contractTypeList.stream().filter(contractType -> contractType.getContractTypeId() == 4)
                .findFirst().orElseThrow();

        assertions.assertThat(actualMonthToMonthContractType.getContractTypeId()).as("Contract type id")
                .isEqualTo(expectedMonthToMonthContractType.getContractTypeId());
        assertions.assertThat(actualMonthToMonthContractType.getContractType())
                .as("Contract type name")
                .isEqualTo(expectedMonthToMonthContractType.getContractType());
        assertions.assertThat(actualMonthToMonthContractType.getTerm())
                .as("Contract type term")
                .isEqualTo(expectedMonthToMonthContractType.getTerm());
        assertions.assertThat(actualMonthToMonthContractType.getIsActive())
                .as("Is contract type active")
                .isEqualTo(expectedMonthToMonthContractType.getIsActive());
        assertions.assertAll();
    }

    @And("^I have quote items$")
    public void iHaveQuoteItems(List<Product> quoteItems) {
        quotePage.setQuoteItems(quoteItems);
    }

    @And("^I post quote via api with$")
    public void iPostQuoteViaApiWith(List<Quote> quoteList) {
        quotePage.postQuote(quoteList.get(0));
    }

    @And("^I post initial order via api$")
    public void iPostInitialOrderViaApi() {

        initialOrderPage.postInitialOrder();


    }
}
