package steps.workbench;

import com.google.inject.Inject;
import common.Assertions;
import common.SessionProvider;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.AccountInfo;
import dto.Address;
import dto.CallSchedule;
import dto.Lead;
import org.junit.Assert;
import pages.workbench.LeadPage;
import setup.RestSpec;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static com.jayway.restassured.RestAssured.given;

public class MergerAssignedCallbacksToWbCallScheduleProcess {

    private final RestSpec restSpec;
    private final AccountInfo accountInfo;
    private final CallSchedule callSchedule;
    private final SessionProvider sessionProvider;
    private final Assertions assertions;
    private final System.Logger logger;
    private final LeadPage leadPage;

    @Inject
    public MergerAssignedCallbacksToWbCallScheduleProcess(RestSpec restSpec, AccountInfo accountInfo, CallSchedule callSchedule, SessionProvider sessionProvider, Assertions assertions, System.Logger logger, LeadPage leadPage) {
        this.restSpec = restSpec;
        this.accountInfo = accountInfo;
        this.callSchedule = callSchedule;
        this.sessionProvider = sessionProvider;
        this.assertions = assertions;
        this.logger = logger;
        this.leadPage = leadPage;
    }

    @Given("^I post a lead via api$")
    public void iPostALeadViaApi() {

        Lead lead = leadPage.postLead();
        logger.log(System.Logger.Level.INFO, ">>>>> Lead generated: " + lead.getLeadId());
        Assert.assertNotEquals("LeadId is 0", 0, lead.getLeadId());
        accountInfo.setLead(lead);
        Address premisesAddress = leadPage.getPremisesAddress(accountInfo.getLead().getLeadId());
        double tax = leadPage.getTaxPercentage(premisesAddress);
        logger.log(System.Logger.Level.INFO, "Tax percentage: " + tax);

    }

    @When("^I post a call back schedule by assigning it to a sales rep$")
    public void iPostACallBackScheduleByAssigningItToASalesRep() {
        given().spec(restSpec.getWbRequestSpecification())
                .body(buildCallScheduleRequestBody())
                .when()
                .post("/api/Lead/" + accountInfo.getLead().getLeadId() + "/InsertCallBackSchedule")
                .then()
                .statusCode(200);
    }

    @Then("^I verify call back schedule record in CallSchedule table$")
    public void iVerifyCallBackScheduleRecordInCallScheduleTable() {
        List<CallSchedule> callScheduleList = sessionProvider.selectFrom(CallSchedule.class)
                .where("leadId").isEqualTo(accountInfo.getLead().getLeadId()).buildQuery().getList(CallSchedule.class);
        CallSchedule latestCallbackSchedule = callScheduleList.stream().sorted((a, b) -> b.getDateCreated().compareTo(a.getDateCreated())).collect(Collectors.toList()).get(0);
        assertions.assertThat(latestCallbackSchedule.getAssignedToId()).as("Assigned to id on callback schedule table")
                .isEqualTo(callSchedule.getAssignedToId());
        assertions.assertThat(latestCallbackSchedule.getCreatedById()).as("Created by id in callback schedule table")
                .isEqualTo(callSchedule.getCreatedById());
        assertions.assertThat(latestCallbackSchedule.getPhone()).as("Phone number in callback schedule table")
                .isEqualTo(callSchedule.getPhone());
        assertions.assertAll();
    }

    @And("^I verify there is only one call back schedule record for the lead generated$")
    public void iVerifyThereIsOnlyOneCallBackScheduleRecordForTheLeadGenerated() {

    }

    private CallSchedule buildCallScheduleRequestBody() {

        callSchedule.setBeginDate(LocalDateTime.now().plusDays(1).format(DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm a")));
        callSchedule.setLeadId(accountInfo.getLead().getLeadId());
        callSchedule.setDialerCall(true);
        callSchedule.setAssignedToId(131635);
        callSchedule.setCreatedById(133456);
        callSchedule.setNote("tes test test");
        callSchedule.setPhone(accountInfo.getLeadRawInfo().getPhone());
        logger.log(System.Logger.Level.INFO, ">>>>>> Posting call back schedule with: " + callSchedule);
        return callSchedule;
    }

}
