package steps.api.customerportal;

import com.github.javafaker.Faker;
import com.google.inject.Inject;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.OrderModel;
import helper.Formatter;
import models.Address;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import pages.customerportal.moveportal.MoversKitDeliveryPage;
import reporting.Reporter;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class VerifyCalculatedShipByDateSteps {

    private final MoversKitDeliveryPage moversKitDeliveryPage;
    private final OrderModel orderModel;
    private final SessionFactory sessionFactory;
    private final System.Logger logger;


    @Inject
    public VerifyCalculatedShipByDateSteps(MoversKitDeliveryPage moversKitDeliveryPage, OrderModel orderModel, SessionFactory sessionFactory, System.Logger logger) {
        this.moversKitDeliveryPage = moversKitDeliveryPage;
        this.orderModel = orderModel;
        this.sessionFactory = sessionFactory;
        this.logger = logger;
    }


    @Given("^I post relocation request with \"([^\"]*)\" date$")
    public void iPostRelocationRequestWithDate(String scheduledDeliveryDate) {

        moversKitDeliveryPage.cancelAllPendingRelocationOrders();

        Address movingAddress = getMovingAddress();

        LocalDate movingDate = Formatter.stringDateToRegularDate.format("Today", LocalDate.now());

        Address deliveryAddress = getDeliveryAddress();

        LocalDate deliveryDate = Formatter.stringDateToRegularDate.format(scheduledDeliveryDate, LocalDate.now());

        Reporter.addStepLog("Posting relocation with scheduled date: " + deliveryDate);

        orderModel.setOrderId(moversKitDeliveryPage.postRelocationRequest(movingAddress, movingDate, deliveryAddress, deliveryDate).getOrderId());
        Reporter.addStepLog("Relocation order created: " + orderModel.getOrderId());

    }

    @And("^I query db for order info$")
    public void iQueryDbForOrderInfo() {
        orderModel.setComputedShipByDate(sessionFactory.openSession().get(orderModel.getClass(), orderModel.getOrderId()).getComputedShipByDate());
        Reporter.addStepLog("OrderId: " + orderModel.getOrderId() + " ComputedShipByDate: " + orderModel.getComputedShipByDate());
    }

    @Then("^I verify scheduled date \"([^\"]*)\" and calculated ship by date \"([^\"]*)\"$")
    public void iVerifyScheduledDateAndCalculatedShipByDate(String scheduledDeliveryDate, String calculatedShipByDate) {
        LocalDate expectedShipByDate = Formatter.stringDateToRegularDate.format(calculatedShipByDate, LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.valueOf(scheduledDeliveryDate.toUpperCase()))));
        Assert.assertEquals(expectedShipByDate, orderModel.getComputedShipByDate());
        moversKitDeliveryPage.cancelAllPendingRelocationOrders();
    }

    @When("^I update scheduled date for relocation order created with \"([^\"]*)\"$")
    public void iUpdateScheduledDateForRelocationOrderCreatedWith(String newScheduledDate) {
        Address movingAddress = getMovingAddress();

        LocalDate movingDate = Formatter.stringDateToRegularDate.format("Today + 2", LocalDate.now());

        Address deliveryAddress = getDeliveryAddress();

        LocalDate deliveryDate = Formatter.stringDateToRegularDate.format(newScheduledDate, LocalDate.now());

        int orderId = moversKitDeliveryPage.updateRelocationRequest(movingAddress, movingDate, deliveryAddress, deliveryDate, orderModel.getOrderId()).getOrderId();

        Assert.assertEquals(orderModel.getOrderId(), orderId);
    }

    private Address getMovingAddress() {
        Address movingAddress = new Address();
        Faker faker = new Faker();
        movingAddress.setAddress1(faker.address().streetAddress());
        movingAddress.setAddress2(faker.address().secondaryAddress());
        movingAddress.setCity("Vienna");
        movingAddress.setState("VA");
        movingAddress.setPostalCode("22182");

        return movingAddress;
    }

    private Address getDeliveryAddress() {
        Address deliveryAddress = new Address();
        Faker faker = new Faker();
        deliveryAddress.setAddress1(faker.address().streetAddress());
        deliveryAddress.setAddress2(faker.address().secondaryAddress());
        deliveryAddress.setCity("Vienna");
        deliveryAddress.setState("VA");
        deliveryAddress.setPostalCode("22182");
        return deliveryAddress;
    }
}
