package steps.api;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.Map;

public class VerifyPackagePriceSteps {

    @Given("^user have weborder cookie$")
    public void user_have_weborder_cookie() throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }

    @When("^user query for offer$")
    public void user_query_for_offer() throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^user gets actual package retail price$")
    public void user_gets_actual_package_retail_price() throws Throwable {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^user calculates expected package retail price with$")
    public void user_calculates_expected_package_retail_price_with(Map<String, Integer> packageMap) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)

    }
}
