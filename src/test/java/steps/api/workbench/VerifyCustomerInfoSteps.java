package steps.api.workbench;

import com.google.inject.Inject;
import common.SessionProvider;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import dto.Account;
import dto.AccountInfo;
import dto.Account_;
import dto.Contact;
import dto.Contact_;
import pages.workbench.CustomerInfoPage;

public class VerifyCustomerInfoSteps {

    private final CustomerInfoPage customerInfoPage;
    private final AccountInfo accountInfo;
    private final SessionProvider sessionProvider;
    private final System.Logger logger;

    @Inject
    public VerifyCustomerInfoSteps(CustomerInfoPage customerInfoPage, AccountInfo accountInfo, SessionProvider sessionProvider, System.Logger logger) {
        this.customerInfoPage = customerInfoPage;
        this.accountInfo = accountInfo;
        this.sessionProvider = sessionProvider;
        this.logger = logger;
    }

    @Given("^I query db for accountId$")
    public void iQueryDbForAccountId() {
        accountInfo.setAccount(sessionProvider.selectFrom(Account.class).where(Account_.LEAD_ID).isEqualTo(accountInfo.getLead().getLeadId())
                .buildQuery().get(Account.class));
    }

    @And("^I query db for personal info with accountId$")
    public void iQueryDbForPersonalInfoWithAccountId() {
        accountInfo.setPersonalInfo(sessionProvider.selectFrom(Contact.class).where(Account_.MONITORING_CONTACT_ID).isEqualTo(accountInfo.getAccount().getMonitoringContactId())
                .buildQuery().get(Contact.class));
        logger.log(System.Logger.Level.INFO, ">>>>>>>>>>>> AccountId: " + accountInfo.getAccount().getAccountId());
    }

    @And("^I query api for personal info with accountId$")
    public void iQueryApiForPersonalInfoWithAccountId() {

    }

    @Then("^I compare personal info from db with api$")
    public void iComparePersonalInfoFromDbWithApi() {

    }
}
