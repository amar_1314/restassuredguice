package helper;

import com.google.inject.Inject;
import common.ApplicationProperties;

public class Navigator {

    private final ApplicationProperties applicationProperties;
    private final Ui ui;

    @Inject
    public Navigator(ApplicationProperties applicationProperties, Ui ui) {
        this.applicationProperties = applicationProperties;
        this.ui = ui;
    }

    public Location cp() {
        return new Location(applicationProperties.cp.url, ui);
    }

    public Location wb() {
        return new Location(applicationProperties.wb.url, ui);
    }

    public Location weborder() {
        return new Location(applicationProperties.weborder.url, ui);
    }
}
