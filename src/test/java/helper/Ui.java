package helper;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.google.inject.Inject;
import com.google.inject.Provider;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import reporting.ReportProperties;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@ScenarioScoped
public class Ui {

    private final By loadingElements = By.cssSelector(".loader [alt='Loading...']");
    private final By loadingMask = By.cssSelector(".loading-mask");
    private By overlay = By.id("overlay-content");

    @Inject
    private Provider<WebDriver> webDriverProvider = null;

    public static void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sleep(int milliSec) {
        try {
            Thread.sleep(milliSec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public WebDriver getDriver() {
        return webDriverProvider.get();
    }


    public void waitForCpLoading() {
        getWait(30)
                .until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("[ng-class='{'position-absolute' : $ctrl.inputOverlay}']")));
    }

    public Wait<WebDriver> getWait(int seconds) {
        return new FluentWait<>(getDriver())
                .withTimeout(seconds, TimeUnit.SECONDS)
                .pollingEvery(50, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .ignoring(InvalidElementStateException.class)
                .ignoring(ElementNotVisibleException.class)
                .ignoring(StaleElementReferenceException.class)
                .ignoring(WebDriverException.class);
    }

    public MediaEntityModelProvider getScreenshot() {
        try {
            return MediaEntityBuilder.createScreenCaptureFromPath(getScreenshotPath()).build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void finishLoading() {
        getWait(120).until(ExpectedConditions.and(
                ExpectedConditions.invisibilityOfElementLocated(loadingElements),
                ExpectedConditions.invisibilityOfElementLocated(overlay),
                ExpectedConditions.invisibilityOfElementLocated(loadingMask)
        ));
    }

    public WebElementExtension findElement(By by) {
        finishLoading();
        return new WebElementExtension(by);
    }

    public List<WebElement> findElements(By by) {
        return findElements(by, false);
    }

    public List<WebElement> findElements(By by, boolean waitForVisibility) {
        finishLoading();
        if (waitForVisibility) {
            return getWait(30).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
        }
        return getWait(30).until(d -> {
            List<WebElement> elements = getDriver().findElements(by).stream().filter(WebElement::isDisplayed).collect(Collectors.toList());
            if (elements.size() > 0) {
                return elements;
            } else {
                return findElements(by, false);
            }
        });
    }

    public WebElementExtension findElement(WebElement element) {
        finishLoading();
        return new WebElementExtension(element);
    }

    private String getScreenshotPath() {
        TakesScreenshot screenshot = ((TakesScreenshot) getDriver());
        File file = screenshot.getScreenshotAs(OutputType.FILE);
        String screenshotsPath = "screenshots/" + System.currentTimeMillis() + ".png";
        try {
            FileUtils.copyFile(file, new File(ReportProperties.INSTANCE.getReportFolder() + "/" + screenshotsPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return System.getProperty("user.dir") + "/" + ReportProperties.INSTANCE.getReportFolder() + "/" + screenshotsPath;
    }

    public class WebElementExtension extends RemoteWebElement {
        private WebElement element;

        WebElementExtension(By by) {
            finishLoading();
            this.element = getWait(45).until(ExpectedConditions.visibilityOfElementLocated(by));
        }

        WebElementExtension(WebElement element) {
            finishLoading();
            this.element = element;
            getWait(45).until(ExpectedConditions.visibilityOf(this.element));
        }

        public WebElement scroll() {
            finishLoading();
            getWait(45).until(ExpectedConditions.visibilityOf(this.element));
            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(false);", element);
            return element;
        }

        public void jsClick() {
            finishLoading();
            scroll();
            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", element);
        }

    }
}
