package helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

public enum TestDataHandler {
    contractTypes("contractTypes.json");

    private String fileName;

    TestDataHandler(String fileName) {
        this.fileName = fileName;
    }

    public <T> List<T> getList(Class<T> t) {
        ObjectMapper mapper = new ObjectMapper();
        List<T> result = null;
        try {
            File testFile = new File("src/test/data/" + this.fileName);
            if (!testFile.exists()) {
                Assert.fail("Test data file " + this.fileName + " not found");
            }
            result = mapper.readValue(testFile, mapper.getTypeFactory().constructCollectionType(List.class, t));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Objects.requireNonNull(result);
    }
}
