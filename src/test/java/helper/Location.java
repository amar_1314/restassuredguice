package helper;

import com.google.inject.Inject;

public class Location {

    private final Ui ui;
    private String baseUrl;

    @Inject
    Location(String baseUrl, Ui ui) {
        this.baseUrl = baseUrl;
        this.ui = ui;
    }

    public void go() {
        ui.getDriver().get(baseUrl);
    }

    public void go(String rel) {
        ui.getDriver().get(baseUrl + rel);
    }
}
