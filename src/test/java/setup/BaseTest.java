package setup;

import com.google.inject.Inject;
import com.google.inject.Provider;
import common.ApplicationProperties;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import dto.AccountInfo;
import dto.Lead;
import dto.LeadRawInfo;
import helper.Ui;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.openqa.selenium.WebDriver;
import reporting.Reporter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class BaseTest {
    private final SessionFactory sessionFactory;
    private final ApplicationProperties applicationProperties;
    private final AccountInfo accountInfo;
    @Inject
    private Provider<WebDriver> driverProvider = null;
    @Inject
    private Provider<TestType> testTypeProvider = null;
    @Inject
    private Provider<Ui> uiProvider = null;


    @Inject
    public BaseTest(SessionFactory sessionFactory, ApplicationProperties applicationProperties, AccountInfo accountInfo) {
        this.sessionFactory = sessionFactory;
        this.applicationProperties = applicationProperties;
        this.accountInfo = accountInfo;
    }

    @Before
    public void before() {
        sessionFactory.openSession();
    }

    @Before(value = "@ui", order = 2)
    public void beforeUi() {
        testTypeProvider.get().setUi(true);
    }

    @Before("@wb")
    public void beforeWb() {
        Session session = sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<LeadRawInfo> criteria = builder.createQuery(LeadRawInfo.class);
        Root<LeadRawInfo> leadRawInfoRoot = criteria.from(LeadRawInfo.class);
        criteria.select(leadRawInfoRoot).where(builder.like(leadRawInfoRoot.get("email"), applicationProperties.wb.testAccount.email));
        accountInfo.setLeadRawInfo(session.createQuery(criteria).getSingleResult());

        CriteriaQuery<Lead> leadCriteria = builder.createQuery(Lead.class);
        Root<Lead> leadRoot = leadCriteria.from(Lead.class);
        leadCriteria.select(leadRoot)
                .where(builder.and(
                        builder.equal(leadRoot.get("rawLeadId"), accountInfo.getLeadRawInfo().getRawLeadId()),
                        builder.isNull(leadRoot.get("parentLeadId"))
                        )
                );
        accountInfo.setLead(session.createQuery(leadCriteria).getSingleResult());

        session.close();
    }


    @After("@ui")
    public void afterUi(Scenario scenario) {
        if (scenario.isFailed()) {
            Reporter.addScreenCapture(uiProvider.get().getScreenshot(), "FAILED");
        } else {
            driverProvider.get().quit();
        }
    }

    @After
    public void after() {
        sessionFactory.close();
    }

}
